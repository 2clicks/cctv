package com.amrasia.app.bma.app;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.amrasia.app.bma.flow.DashboardActivity;

/**
 * 
 * @author BabeDev
 * 
 */
public class CctvFragment extends Fragment {

    protected DashboardActivity getCctvActivity() {
        return (DashboardActivity) getActivity();
    }

    protected CctvApplication getApp() {
        return getCctvActivity().getApp();
    }

    protected void hideKeyboard(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}
