package com.amrasia.app.bma.dao;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.amrasia.app.bma.app.CctvApplication;
import com.amrasia.app.bma.model.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * 
 * @author BabeDev
 * 
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private Context context;

    public DatabaseHelper(Context context) {
        super(context, "cctv.db", null, 2);
        this.context = context;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        Class[] clazzes = {User.class};

        for (Class clazz : clazzes) {
            try {
                TableUtils.createTable(connectionSource, clazz);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        Dao<User, String> userDao = ((CctvApplication) context.getApplicationContext()).getUserDao();

        if (oldVersion == 1) {
            try {
                userDao.executeRawNoArgs("ALTER TABLE 'user' ADD COLUMN 'id' VARCHAR");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
