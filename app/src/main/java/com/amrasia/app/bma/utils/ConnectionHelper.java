package com.amrasia.app.bma.utils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * 
 * @author BabeDev
 *
 */
public final class ConnectionHelper {

    /**
     * Since API send a stupid data and they will not correct it so I have to
     * replace '//images' to '/images' by myself using Thai language for file
     * name.... How stupid they are
     * 
     * @param path
     *            URL path
     * @return Encoded path
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    public static URL encodeUrlWithThaiChar(String path) throws UnsupportedEncodingException, MalformedURLException {
        path = path.replaceAll("//images", "/images");
        path = URLEncoder.encode(path, "UTF-8");
        path = path.replaceAll("%3A", ":");
        path = path.replaceAll("%2F", "/");
        path = path.replace("+", "%20");

        return new URL(path);
    }
}
