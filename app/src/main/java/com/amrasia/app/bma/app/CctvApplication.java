package com.amrasia.app.bma.app;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Application;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.dao.DatabaseHelper;
import com.amrasia.app.bma.model.Area;
import com.amrasia.app.bma.model.Cctv;
import com.amrasia.app.bma.model.User;
import com.amrasia.app.bma.model.Zone;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

/**
 * 
 * @author BabeDev
 * 
 */
public class CctvApplication extends Application {

    public enum TrackerName {
        APP_TRACKER, GLOBAL_TRACKER, ECOMMERCE_TRACKER
    }

    private User user;

    private List<Area> areas;
    private List<Zone> zones;
    
    private List<Cctv> cctvs;
    private List<Cctv> events;
    private List<Cctv> news;

    private DatabaseHelper databaseHelper;
    private Dao<User, String> userDao;

    private Locale locale;

    private HashMap<TrackerName, Tracker> trackers;

    @Override
    public void onCreate() {
        super.onCreate();

        areas = new ArrayList<Area>();
        zones = new ArrayList<Zone>();
        cctvs = new ArrayList<Cctv>();
        events = new ArrayList<Cctv>();
        news = new ArrayList<Cctv>();
        
        trackers = new HashMap<TrackerName, Tracker>();
        databaseHelper = new DatabaseHelper(this);

        Dao<User, String> dao = getUserDao();

        try {
            List<User> users = dao.queryForAll();

            if (users != null && !users.isEmpty()) {
                user = users.get(0);
            }
        } catch (SQLException e) {

        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public List<Cctv> getCctvs() {
		return cctvs;
	}

	public List<Cctv> getEvents() {
		return events;
	}

	public List<Cctv> getNews() {
		return news;
	}

	public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Dao<User, String> getUserDao() {
        if (userDao == null) {
            try {
                userDao = databaseHelper.getDao(User.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return userDao;
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!trackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                            : analytics.newTracker(R.xml.ecommerce_tracker);
            trackers.put(trackerId, t);

        }
        return trackers.get(trackerId);
    }
}
