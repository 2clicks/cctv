package com.amrasia.app.bma.flow;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Event;
import com.amrasia.app.bma.utils.ConnectionHelper;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author BabeDev
 * 
 */
public class EventDetailFragment extends CctvFragment {

    private ImageView imageView;
    private TextView titleView;
    private TextView detailView;
    private TextView referView;
    private ImageView poiView;

    private String id;

    private Event detail;

    public EventDetailFragment() {

    }

    public EventDetailFragment(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_detail, container, false);
        imageView = (ImageView) view.findViewById(R.id.image);
        titleView = (TextView) view.findViewById(R.id.title);
        detailView = (TextView) view.findViewById(R.id.detail);
        referView = (TextView) view.findViewById(R.id.refer);
        poiView = (ImageView) view.findViewById(R.id.poi);

        view.findViewById(R.id.back).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                getCctvActivity().openEventList();
            }
        });

        new GetInfoTask().execute(id);

        return view;
    }

    private class GetInfoTask extends AsyncTask<String, Void, Event> {

        @Override
        protected Event doInBackground(String... params) {
            return SoapConnector.getEvent(params[0]);
        }

        @Override
        protected void onPostExecute(Event result) {
            if (result != null) {
                detail = result;

                titleView.setText(result.getHeadline());
                detailView.setText(result.getDetail());
                referView.setText(result.getRefer());

                switch (detail.getType()) {
                case 0:
                    poiView.setImageResource(R.drawable.ic_event_all);
                    break;
                case 1:
                    poiView.setImageResource(R.drawable.ic_event_hit);
                    break;
                case 2:
                    poiView.setImageResource(R.drawable.ic_event_mob);
                    break;
                case 3:
                    poiView.setImageResource(R.drawable.ic_event_fix);
                    break;
                case 4:
                    poiView.setImageResource(R.drawable.ic_event_road);
                    break;
                case 5:
                    poiView.setImageResource(R.drawable.ic_event_traffic);
                    break;
                case 6:
                    poiView.setImageResource(R.drawable.ic_event_construct);
                    break;
                case 7:
                    poiView.setImageResource(R.drawable.ic_event_repair);
                    break;
                case 8:
                    poiView.setImageResource(R.drawable.ic_event_fire);
                    break;
                case 9:
                    poiView.setImageResource(R.drawable.ic_event_rain);
                    break;
                }

                poiView.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        if (detail != null) {
                            Bitmap bitmap = null;

                            if (imageView.getDrawable() != null) {
                                bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                            }

                            getCctvActivity().openMap(id, detail.getType(), detail.getLatitude(),
                                    detail.getLongitude(), MapFragment.TYPE_EVENT, detail.getHeadline(),
                                    detail.getDetail(), bitmap);
                        }
                    }
                });

                try {
                    Picasso.with(getActivity()).load(Uri.parse(ConnectionHelper.encodeUrlWithThaiChar(result.getImagePath()).toURI().toString())).into(imageView, new com.squareup.picasso.Callback() {

                        @Override
                        public void onError() {
                            Picasso.with(getActivity()).load(R.drawable.no_avatar).into(imageView);
                        }

                        @Override
                        public void onSuccess() {
                            // Do nothing
                        }
                        
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
