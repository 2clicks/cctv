package com.amrasia.app.bma.flow;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Area;
import com.amrasia.app.bma.model.User;
import com.android.camera.CropImageIntentBuilder;
import com.j256.ormlite.dao.Dao;

/**
 * 
 * @author BabeDev
 * 
 */
public class MemberFragment extends CctvFragment implements OnClickListener {

    public static final int REQ_GALLERY = 99;
    public static final int REQ_CROP = 98;

    private static final int IMAGE_SIZE = 612;

    private View rootView;
    private View profileLayout;
    private View loginLayout;
    private View registerLayout;
    private View routeLayout;
    private View forgetPasswordLayout;
    private View forgetPasswordRequestLayout;
    private View forgetPasswordSucceedLayout;
    private View forgetPasswordFailedLayout;

    private LinearLayout roadsLayout;
    private LinearLayout profileAreaLayout;

    private TextView generalInfoView;
    private TextView subscribeView;
    private TextView routeView;

    private View generalInfoLayout;
    private View subscribeLayout;

    private EditText usernameLoginView;
    private EditText passwordLoginView;

    private EditText firstNameView;
    private EditText lastNameView;
    private EditText pidView;
    private EditText careerView;
    private EditText emailView;
    private EditText usernameView;
    private EditText passwordView;
    private EditText confirmPasswordView;
    private EditText usernameForgetView;
    private Button dobView;
    private Spinner genderView;
    private ImageView profileImageView;
    private Button submitBtn;
    private Button cancelBtn;

    private TextView profileUsernameView;
    private TextView profileNameView;
    private TextView profilePidView;
    private TextView profileGenderView;
    private TextView profileOccupationView;
    private TextView profileDobView;
    private TextView profileEmailView;
    private TextView profileNewsView;

    private ImageView formTitleView;
    private ImageView userImageView;

    private RadioGroup newsChoiceView;

    private Dao<User, String> dao;

    private List<Boolean> roadSelects;
    private String newsTypeSelected;

    private Uri imageUri;
    private boolean isEditMode;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.member, container, false);

        profileLayout = rootView.findViewById(R.id.profile_layout);
        loginLayout = rootView.findViewById(R.id.login_layout);
        registerLayout = rootView.findViewById(R.id.register_layout);
        generalInfoLayout = rootView.findViewById(R.id.general_info_layout);
        subscribeLayout = rootView.findViewById(R.id.subscribe_layout);
        routeLayout = rootView.findViewById(R.id.route_layout);
        forgetPasswordLayout = rootView.findViewById(R.id.forget_password_layout);
        forgetPasswordRequestLayout = rootView.findViewById(R.id.forget_password_request_layout);
        forgetPasswordSucceedLayout = rootView.findViewById(R.id.forget_password_succeed_layout);
        forgetPasswordFailedLayout = rootView.findViewById(R.id.forget_password_failed_layout);

        roadsLayout = (LinearLayout) rootView.findViewById(R.id.roads_layout);

        generalInfoView = (TextView) rootView.findViewById(R.id.general_info);
        subscribeView = (TextView) rootView.findViewById(R.id.subscribe);
        routeView = (TextView) rootView.findViewById(R.id.route);

        usernameLoginView = (EditText) rootView.findViewById(R.id.username_txt);
        passwordLoginView = (EditText) rootView.findViewById(R.id.password_txt);

        usernameForgetView = (EditText) rootView.findViewById(R.id.username_forget_txt);

        firstNameView = (EditText) rootView.findViewById(R.id.firstname);
        lastNameView = (EditText) rootView.findViewById(R.id.lastname);
        pidView = (EditText) rootView.findViewById(R.id.pid);
        careerView = (EditText) rootView.findViewById(R.id.career);
        emailView = (EditText) rootView.findViewById(R.id.email);
        usernameView = (EditText) rootView.findViewById(R.id.username);
        passwordView = (EditText) rootView.findViewById(R.id.password);
        confirmPasswordView = (EditText) rootView.findViewById(R.id.password_confirm);
        dobView = (Button) rootView.findViewById(R.id.dob);
        genderView = (Spinner) rootView.findViewById(R.id.gender);
        profileImageView = (ImageView) rootView.findViewById(R.id.image_profile);
        newsChoiceView = (RadioGroup) rootView.findViewById(R.id.news_choice);
        submitBtn = (Button) rootView.findViewById(R.id.submit_btn);
        cancelBtn = (Button) rootView.findViewById(R.id.cancel_btn);

        formTitleView = (ImageView) rootView.findViewById(R.id.form_title);
        userImageView = (ImageView) rootView.findViewById(R.id.user_image);

        profileUsernameView = (TextView) rootView.findViewById(R.id.profile_username);
        profileNameView = (TextView) rootView.findViewById(R.id.profile_name);
        profilePidView = (TextView) rootView.findViewById(R.id.profile_pid);
        profileGenderView = (TextView) rootView.findViewById(R.id.profile_gender);
        profileOccupationView = (TextView) rootView.findViewById(R.id.profile_occupation);
        profileDobView = (TextView) rootView.findViewById(R.id.profile_dob);
        profileEmailView = (TextView) rootView.findViewById(R.id.profile_email);
        profileNewsView = (TextView) rootView.findViewById(R.id.profile_news);
        profileAreaLayout = (LinearLayout) rootView.findViewById(R.id.profile_area_layout);

        dao = getApp().getUserDao();
        roadSelects = new ArrayList<Boolean>();

        rootView.findViewById(R.id.register).setOnClickListener(this);
        rootView.findViewById(R.id.forget_password).setOnClickListener(this);
        rootView.findViewById(R.id.login_btn).setOnClickListener(this);
        rootView.findViewById(R.id.logout_btn).setOnClickListener(this);
        rootView.findViewById(R.id.edit_btn).setOnClickListener(this);
        rootView.findViewById(R.id.forget_password_btn).setOnClickListener(this);
        rootView.findViewById(R.id.forget_password_back_btn).setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        dobView.setOnClickListener(this);
        profileImageView.setOnClickListener(this);

        generalInfoView.setOnClickListener(new ChangeTabListener());
        subscribeView.setOnClickListener(new ChangeTabListener());
        routeView.setOnClickListener(new ChangeTabListener());

        newsChoiceView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.news_choice_all) {
                    newsTypeSelected = "1";
                } else if (checkedId == R.id.news_choice_public) {
                    newsTypeSelected = "2";
                } else if (checkedId == R.id.news_choice_event) {
                    newsTypeSelected = "3";
                } else if (checkedId == R.id.news_choice_none) {
                    newsTypeSelected = "4";
                }
            }
        });

        newsChoiceView.check(R.id.news_choice_all);

        // Prepare roads
        //
        int pos = 0;
        boolean isEng = getApp().getLocale() == Locale.ENGLISH;
        
        for (Area area : getApp().getAreas()) {
            final int item = pos++;

            CheckBox roadBox = (CheckBox) inflater.inflate(R.layout.road_item, roadsLayout, false);
            roadBox.setText(isEng ? area.getNameEn().trim() : area.getName().trim());
            roadBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    roadSelects.set(item, isChecked);
                }
            });

            roadsLayout.addView(roadBox);
            roadSelects.add(false);
        }

        // Check existing user
        // Profile layout unused since requirement had changed but I had not remove it yet
        //
        if (getApp().getUser() != null) {
            setUpMemberForm();
        } else {
            profileLayout.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
        }

        setUpHideKeyboard();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_GALLERY) {
                imageUri = Uri.fromFile(new File(getActivity().getCacheDir(), "temp.jpg"));
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(IMAGE_SIZE, IMAGE_SIZE, imageUri);
                cropImage.setSourceImage(data.getData());
                startActivityForResult(cropImage.getIntent(getActivity()), REQ_CROP);
            } else if (requestCode == REQ_CROP) {
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(imageUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    profileImageView.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn) {
            hideKeyboard(passwordLoginView);

            if (usernameLoginView.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), R.string.dialog_validate_username, Toast.LENGTH_LONG).show();
            } else if (passwordLoginView.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), R.string.dialog_validate_password, Toast.LENGTH_LONG).show();
            } else {
                new LoginTask().execute();
            }
        } else if (v.getId() == R.id.register) {
            isEditMode = false;
            usernameView.setEnabled(true);

            loginLayout.setVisibility(View.GONE);
            registerLayout.setVisibility(View.VISIBLE);
            formTitleView.setImageResource(R.drawable.register_title);
            submitBtn.setText(R.string.lbl_register);

            generalInfoView.callOnClick();
        } else if (v.getId() == R.id.forget_password) {
            loginLayout.setVisibility(View.GONE);
            forgetPasswordLayout.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.submit_btn) {
            String errorMessage = getString(validate());

            if (errorMessage.isEmpty()) {
                new SendUserTask().execute();
            } else {
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
            }
        } else if (v.getId() == R.id.cancel_btn) {
            registerLayout.setVisibility(View.GONE);

            if (isEditMode) {
                profileLayout.setVisibility(View.VISIBLE);
            } else {
                loginLayout.setVisibility(View.VISIBLE);
            }
        } else if (v.getId() == R.id.image_profile) {
            Intent intent = new Intent(Intent.ACTION_PICK);

            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                intent.setType("image/*");
                startActivityForResult(intent, REQ_GALLERY);
            }
        } else if (v.getId() == R.id.dob) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd - MM - yyyy");

            Calendar calendar = Calendar.getInstance();

            try {
                calendar.setTime(sdf.parse(dobView.getText().toString()));
            } catch (ParseException e) {

            }

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String date = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                    String month = ++monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;

                    dobView.setText(date + " - " + month + " - " + year);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            dialog.getDatePicker().setCalendarViewShown(false);
            dialog.show();
        } else if (v.getId() == R.id.forget_password_btn) {
            hideKeyboard(usernameForgetView);
            new ForgetPasswordTask().execute();
        } else if (v.getId() == R.id.forget_password_back_btn) {
            forgetPasswordRequestLayout.setVisibility(View.VISIBLE);
            forgetPasswordSucceedLayout.setVisibility(View.GONE);
            forgetPasswordFailedLayout.setVisibility(View.GONE);
        } else if (v.getId() == R.id.edit_btn) {
            isEditMode = true;
            usernameView.setEnabled(false);
            profileLayout.setVisibility(View.GONE);
            registerLayout.setVisibility(View.VISIBLE);
            formTitleView.setImageResource(R.drawable.edit_title);
            submitBtn.setText(R.string.lbl_submit);
            generalInfoView.callOnClick();
        } else if (v.getId() == R.id.logout_btn) {
            try {
                getApp().setUser(null);
                dao.delete(dao.queryForAll());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            usernameView.setText("");
            firstNameView.setText("");
            lastNameView.setText("");
            pidView.setText("");
            careerView.setText("");
            dobView.setText("");
            emailView.setText("");
            profileImageView.setImageResource(R.drawable.no_avatar);
            userImageView.setImageResource(R.drawable.no_avatar);
            newsChoiceView.check(R.id.news_choice_all);

            File file = new File(getActivity().getCacheDir() + "profile.png");

            if (file.exists()) {
                file.delete();
            }

            for (int i = 0; i < getApp().getAreas().size(); ++i) {
                ((CheckBox) roadsLayout.getChildAt(i)).setChecked(false);
                roadSelects.set(i, false);
            }

            profileLayout.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
        }
    }

    private void hideKeyboard() {
        EditText[] editTexts = {usernameView, passwordView, firstNameView, lastNameView, pidView, careerView,
                passwordView, confirmPasswordView};

        for (EditText editText : editTexts) {
            hideKeyboard(editText);
        }
    }

    private void setUpHideKeyboard() {
        int[] ids = {R.id.register_layout, R.id.username_view, R.id.firstname_view, R.id.lastname_view, R.id.pid_view,
                R.id.career_view, R.id.dob_view, R.id.password_view, R.id.confirm_password_view, R.id.gender_view,
                R.id.image_profile_view};

        // Touch outer area to hide keyboard
        //
        for (int id : ids) {
            rootView.findViewById(id).setOnTouchListener(new AutoHideKeyboardListener());
        }
    }

    private void setUpMemberForm() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd - MM - yyyy");
        User user = getApp().getUser();

        usernameView.setText(user.getName());
        firstNameView.setText(user.getFirstName());
        lastNameView.setText(user.getLastName());
        pidView.setText(user.getPid());
        careerView.setText(user.getCareer());
        dobView.setText(sdf.format(user.getDob()));
        emailView.setText(user.getEmail());

        profileUsernameView.setText(user.getName());
        profileNameView.setText(user.getFirstName() + " " + user.getLastName());
        profilePidView.setText(user.getPid());
        profileGenderView.setText(user.getGender().equals("1") ? R.string.lbl_male : R.string.lbl_female);
        profileOccupationView.setText(user.getCareer());
        profileDobView.setText(sdf.format(user.getDob()));
        profileEmailView.setText(user.getEmail());

        profileAreaLayout.removeAllViews();

        // Set up gender
        // Might fix later cause API may change gender ID in the future
        //
        genderView.setSelection(Integer.parseInt(user.getGender()));

        // Set up news type
        //
        if (user.getNewsType().equalsIgnoreCase("1")) {
            newsChoiceView.check(R.id.news_choice_all);
            profileNewsView.setText(R.string.lbl_news_all);
        } else if (user.getNewsType().equalsIgnoreCase("2")) {
            newsChoiceView.check(R.id.news_choice_public);
            profileNewsView.setText(R.string.lbl_news_public);
        } else if (user.getNewsType().equalsIgnoreCase("3")) {
            newsChoiceView.check(R.id.news_choice_event);
            profileNewsView.setText(R.string.lbl_news_event);
        } else if (user.getNewsType().equalsIgnoreCase("4")) {
            newsChoiceView.check(R.id.news_choice_none);
            profileNewsView.setText(R.string.lbl_news_none);
        }

        // Set up road list
        // Should improve looping algorithm later
        //
        String[] roads = user.getRoads().split(",");
        int areaSize = getApp().getAreas().size();
        boolean isEng = getApp().getLocale() == Locale.ENGLISH;
        
        for (String road : roads) {
            for (int i = 0; i < areaSize; ++i) {
                if (getApp().getAreas().get(i).getId().equalsIgnoreCase(road)) {
                    ((CheckBox) roadsLayout.getChildAt(i)).setChecked(true);
                    roadSelects.set(i, true);

                    View row = getActivity().getLayoutInflater().inflate(R.layout.profile_item, profileAreaLayout,
                            false);
                    ((TextView) row.findViewById(R.id.title)).setText(isEng ? getApp().getAreas().get(i).getNameEn() : getApp().getAreas().get(i).getName());
                    profileAreaLayout.addView(row);

                    break;
                }
            }
        }

        // Set up profile image
        //
        File file = new File(getActivity().getCacheDir() + "profile.png");

        if (file.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

            userImageView.setImageBitmap(bitmap);
            profileImageView.setImageBitmap(bitmap);

            userImageView.setEnabled(false);
        }
    }

    /**
     * Validate member form
     * 
     * @return Resource ID
     */
    private int validate() {
        Matcher emailMatcher = Pattern.compile(
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(
                emailView.getText().toString());

        if (usernameView.getText().toString().isEmpty()) {
            usernameView.requestFocus();
            return R.string.dialog_validate_username;
        } else if (firstNameView.getText().toString().isEmpty()) {
            firstNameView.requestFocus();
            return R.string.dialog_validate_firstname;
        } else if (lastNameView.getText().toString().isEmpty()) {
            lastNameView.requestFocus();
            return R.string.dialog_validate_lastname;
        } else if (!validatePid(pidView.getText().toString())) {
            pidView.requestFocus();
            return R.string.dialog_validate_pid;
        } else if (careerView.getText().toString().isEmpty()) {
            careerView.requestFocus();
            return R.string.dialog_validate_career;
        } else if (dobView.getText().toString().isEmpty()) {
            dobView.requestFocus();
            return R.string.dialog_validate_dob;
        } else if (!emailMatcher.matches()) {
            emailView.requestFocus();
            return R.string.dialog_validate_email;
        } else if (passwordView.getText().toString().isEmpty() && !isEditMode) {
            passwordView.requestFocus();
            return R.string.dialog_validate_password;
        } else if (!passwordView.getText().toString().isEmpty() && confirmPasswordView.getText().toString().isEmpty()) {
            confirmPasswordView.requestFocus();
            return R.string.dialog_validate_confirm_password;
        } else if (!passwordView.getText().toString().equalsIgnoreCase(confirmPasswordView.getText().toString())) {
            confirmPasswordView.requestFocus();
            return R.string.dialog_validate_confirm_password_not_match;
        } else if (genderView.getSelectedItemPosition() == 0) {
            genderView.requestFocus();
            return R.string.dialog_validate_gender;
        }

        return R.string.dialog_validate_empty;
    }

    private boolean validatePid(String pid) {
        if (!pid.isEmpty() && pid.length() == 13) {
            int sum = 0;
            String last = "";
            
            for (int i = 0, j = 13; i < pid.length() - 1; ++i, --j) {
                sum += Integer.parseInt(pid.substring(i, i + 1)) * j;
            }
            
            int mod = sum % 11;
            
            if (mod == 0) {
                last = "1";
            } else if (mod == 1) {
                last = "0";
            } else {
                last = String.valueOf(11 - mod);
            }
            
            return last.equals(pid.substring(12, 13));
        }
        
        return false;
    }
    
    private class LoginTask extends AsyncTask<Void, Void, User> {

        @Override
        protected User doInBackground(Void... params) {
            return SoapConnector.login(usernameLoginView.getText().toString(),
                    Base64.encodeToString(passwordLoginView.getText().toString().getBytes(), Base64.DEFAULT).trim());
        }

        @Override
        protected void onPostExecute(User result) {
            if (result != null) {
                try {
                    dao.delete(dao.queryForAll());
                    dao.create(result);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                getApp().setUser(result);
                usernameLoginView.setText("");
                passwordLoginView.setText("");
                loginLayout.setVisibility(View.GONE);
                profileLayout.setVisibility(View.VISIBLE);

                setUpMemberForm();

                new GetProfileTask().execute(result.getProfileImage());
            } else {
                passwordLoginView.setText("");
                Toast.makeText(getActivity(), R.string.dialog_login_failed, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class SendUserTask extends AsyncTask<Void, Void, Boolean> {

        private String password;
        private User user;
        private Bitmap bitmap;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), "", "Sending data...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // Encode profile image
            //
            bitmap = ((BitmapDrawable) profileImageView.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] image = stream.toByteArray();
            String encodedImage = Base64.encodeToString(image, Base64.DEFAULT);

            // Prepare road list
            //
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < roadSelects.size(); ++i) {
                if (roadSelects.get(i)) {
                    builder.append(getApp().getAreas().get(i).getId());
                    builder.append(",");
                }
            }

            String roads = builder.toString();

            user = new User();
            user.setFirstName(firstNameView.getText().toString());
            user.setLastName(lastNameView.getText().toString());
            user.setPid(pidView.getText().toString());
            user.setCareer(careerView.getText().toString());
            user.setEmail(emailView.getText().toString());
            user.setName(usernameView.getText().toString());
            user.setProfileImage(encodedImage);
            user.setNewsType(newsTypeSelected);
            user.setGender(genderView.getSelectedItemPosition() + "");
            user.setRoads(roads.isEmpty() ? "" : roads.substring(0, roads.length() - 1));

            // If in edit mode and new password is not assign then use the old password
            //
            if (isEditMode && passwordView.getText().toString().isEmpty()) {
                password = getApp().getUser().getPassword();
            } else {
                password = passwordView.getText().toString();
            }

            user.setPassword(Base64.encodeToString(password.getBytes(), Base64.DEFAULT).trim());

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd - MM - yyyy");
                user.setDob(sdf.parse(dobView.getText().toString()));
            } catch (ParseException e) {
                user.setDob(new Date());
            }

            if (isEditMode) {
                user.setId(getApp().getUser().getId());
                return SoapConnector.updateMember(user);
            } else {
                return SoapConnector.registerMember(user);
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();

            if (result) {
                try {
                    // Set decoded password
                    //
                    user.setPassword(password);
                    dao.createOrUpdate(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                getApp().setUser(user);

                if (isEditMode) {
                    Toast.makeText(getActivity(), R.string.dialog_profile_updated, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), R.string.dialog_profile_registered, Toast.LENGTH_LONG).show();
                }

                // Save profile image
                //
                File file = new File(getActivity().getCacheDir() + "profile.png");

                if (file.exists()) {
                    file.delete();
                }

                try {
                    FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
                    bitmap.compress(Bitmap.CompressFormat.PNG, 80, out);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                userImageView.setImageBitmap(bitmap);
                profileImageView.setImageBitmap(bitmap);

                registerLayout.setVisibility(View.GONE);

                if (isEditMode) {
                    profileLayout.setVisibility(View.VISIBLE);
                } else {
                    loginLayout.setVisibility(View.VISIBLE);
                }

                setUpMemberForm();
            } else {
                if (isEditMode) {
                    Toast.makeText(getActivity(), "Update failed", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Register failed", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private class GetProfileTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                return BitmapFactory.decodeStream(connection.getInputStream());
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                File file = new File(getActivity().getCacheDir() + "profile.png");

                if (file.exists()) {
                    file.delete();
                }

                try {
                    FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
                    result.compress(Bitmap.CompressFormat.PNG, 80, out);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                userImageView.setImageBitmap(result);
                profileImageView.setImageBitmap(result);
            }
        }
    }

    private class ForgetPasswordTask extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "", "Requesting password...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return SoapConnector.forgetPassword(usernameForgetView.getText().toString());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();

            forgetPasswordRequestLayout.setVisibility(View.GONE);
            usernameForgetView.setText("");

            if (result) {
                forgetPasswordSucceedLayout.setVisibility(View.VISIBLE);
            } else {
                forgetPasswordFailedLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private class ChangeTabListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            hideKeyboard();

            generalInfoView.setBackgroundResource(R.drawable.bg_tab_not_select);
            subscribeView.setBackgroundResource(R.drawable.bg_tab_not_select);
            routeView.setBackgroundResource(R.drawable.bg_tab_not_select);

            generalInfoView.setTextColor(getResources().getColor(android.R.color.black));
            subscribeView.setTextColor(getResources().getColor(android.R.color.black));
            routeView.setTextColor(getResources().getColor(android.R.color.black));

            generalInfoLayout.setVisibility(View.GONE);
            subscribeLayout.setVisibility(View.GONE);
            routeLayout.setVisibility(View.GONE);

            if (v.getId() == R.id.general_info) {
                generalInfoView.setBackgroundResource(R.drawable.bg_tab_select);
                generalInfoView.setTextColor(getResources().getColor(R.color.green));
                generalInfoLayout.setVisibility(View.VISIBLE);
            } else if (v.getId() == R.id.subscribe) {
                subscribeView.setBackgroundResource(R.drawable.bg_tab_select);
                subscribeView.setTextColor(getResources().getColor(R.color.green));
                subscribeLayout.setVisibility(View.VISIBLE);
            } else if (v.getId() == R.id.route) {
                routeView.setBackgroundResource(R.drawable.bg_tab_select);
                routeView.setTextColor(getResources().getColor(R.color.green));
                routeLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private class AutoHideKeyboardListener implements OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return false;
        }
    }
}
