package com.amrasia.app.bma.flow;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.News;
import com.amrasia.app.bma.utils.ConnectionHelper;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author BabeDev
 * 
 */
public class InfoDetailFragment extends CctvFragment {

    private ImageView imageView;
    private TextView titleView;
    private TextView detailView;
    private TextView referView;

    private String id;

    private News detail;

    public InfoDetailFragment() {

    }

    public InfoDetailFragment(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_detail, container, false);
        imageView = (ImageView) view.findViewById(R.id.image);
        titleView = (TextView) view.findViewById(R.id.title);
        detailView = (TextView) view.findViewById(R.id.detail);
        referView = (TextView) view.findViewById(R.id.refer);

        view.findViewById(R.id.poi).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (detail != null) {
                    Bitmap bitmap = null;

                    if (imageView.getDrawable() != null) {
                        bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                    }

                    getCctvActivity().openMap(id, detail.getLatitude(), detail.getLongitude(), MapFragment.TYPE_INFO,
                            detail.getHeadline(), detail.getDetail(), bitmap);
                }
            }
        });

        view.findViewById(R.id.back).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                getCctvActivity().openInfoList();
            }
        });

        new GetNewsTask().execute(id);

        return view;
    }

    private class GetNewsTask extends AsyncTask<String, Void, News> {

        @Override
        protected News doInBackground(String... params) {
            return SoapConnector.getNews(params[0]);
        }

        @Override
        protected void onPostExecute(News result) {
            if (result != null) {
                detail = result;

                titleView.setText(result.getHeadline());
                detailView.setText(result.getDetail());
                referView.setText(result.getRefer());

                try {
                    Picasso.with(getActivity()).load(Uri.parse(ConnectionHelper.encodeUrlWithThaiChar(result.getImagePath()).toURI().toString())).into(imageView, new com.squareup.picasso.Callback() {

                        @Override
                        public void onError() {
                            Picasso.with(getActivity()).load(R.drawable.no_avatar).into(imageView);
                        }

                        @Override
                        public void onSuccess() {
                            // Do nothing
                        }
                        
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
