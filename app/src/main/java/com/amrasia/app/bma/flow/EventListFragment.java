package com.amrasia.app.bma.flow;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Area;
import com.amrasia.app.bma.model.Event;
import com.amrasia.app.bma.utils.CctvConstant;
import com.amrasia.app.bma.utils.ConnectionHelper;
import com.amrasia.app.bma.utils.ImageHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * 
 * @author BabeDev
 * 
 */
public class EventListFragment extends CctvFragment implements OnMapReadyCallback {

    private static final int REQ_CAMERA = 98;
    private static final int REQ_GALLERY = 99;

    private static final int IMAGE_SIZE = 500;
    private static final int THUMBNAIL_SIZE = 150;

    private View areaListLayout;
    private View listLayout;
    private View addLayout;
    private View reviewLayout;
    private View resultLayout;
    private View geoLayout;
    private View listView;
    private View dataNotFoundView;
    private LinearLayout eventsLayout;
    private LinearLayout areasLayout;

    private TextView eventTypeView;
    private ImageView mediaBtn;
    private ImageView geoBtn;
    private ImageView areaBtn;

    private EditText titleEditView;
    private EditText detailEditView;
    private EditText referEditView;
    private TextView latEditView;
    private TextView lngEditView;
    private TextView areaEditView;

    private TextView latSelectView;
    private TextView lngSelectView;

    private TextView titleView;
    private TextView detailView;
    private TextView referView;
    private TextView areaView;
    private TextView typeView;
    private TextView latitudeView;
    private TextView longitudeView;

    private ImageView imageView;
    private ImageView thumbnailView;

    private ImageView resultIconView;
    private TextView resultTextView;
    private View homeBtn;
    private View backBtn;
    private View backFormBtn;
    private View backGeoBtn;
    private View saveGeoBtn;

    private String[] eventTypeValues;
    private Uri photoUri;

    private Bitmap photoBitmap;
    private String eventType = "";
    private String roadId = "0";
    private double latitude = CctvConstant.DEFAULT_LATITUDE;
    private double longitude = CctvConstant.DEFAULT_LONGITUDE;

    private GoogleMap map;
    private MarkerOptions currentMarker;
    
    private View view;

    private int[] eventBtns = new int[] {R.id.all_btn, R.id.hit_btn, R.id.mob_btn, R.id.fix_btn, R.id.road_btn,
            R.id.traffic_btn, R.id.construct_btn, R.id.repair_btn, R.id.fire_btn, R.id.rain_btn};

    private int[] eventImages = new int[] {R.drawable.ic_event_all, R.drawable.ic_event_hit, R.drawable.ic_event_mob,
            R.drawable.ic_event_fix, R.drawable.ic_event_road, R.drawable.ic_event_traffic,
            R.drawable.ic_event_construct, R.drawable.ic_event_repair, R.drawable.ic_event_fire,
            R.drawable.ic_event_rain};

    private int[] eventSelectedImages = new int[] {R.drawable.ic_event_all_selected, R.drawable.ic_event_hit_selected,
            R.drawable.ic_event_mob_selected, R.drawable.ic_event_fix_selected, R.drawable.ic_event_road_selected,
            R.drawable.ic_event_traffic_selected, R.drawable.ic_event_construct_selected,
            R.drawable.ic_event_repair_selected, R.drawable.ic_event_fire_selected, R.drawable.ic_event_rain_selected};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.event_list, container, false);
        areaListLayout = view.findViewById(R.id.area_list_layout);
        listLayout = view.findViewById(R.id.list_layout);
        addLayout = view.findViewById(R.id.add_layout);
        reviewLayout = view.findViewById(R.id.review_layout);
        resultLayout = view.findViewById(R.id.result_layout);
        geoLayout = view.findViewById(R.id.geo_layout);
        listView = view.findViewById(R.id.list_view);
        dataNotFoundView = view.findViewById(R.id.data_not_found);
        eventsLayout = (LinearLayout) view.findViewById(R.id.events_layout);
        areasLayout = (LinearLayout) view.findViewById(R.id.areas_layout);

        eventTypeView = (TextView) view.findViewById(R.id.event_type_btn);
        mediaBtn = (ImageView) view.findViewById(R.id.image_btn);
        geoBtn = (ImageView) view.findViewById(R.id.geo_btn);
        areaBtn = (ImageView) view.findViewById(R.id.area_btn);

        titleEditView = (EditText) view.findViewById(R.id.title_edit);
        detailEditView = (EditText) view.findViewById(R.id.detail_edit);
        referEditView = (EditText) view.findViewById(R.id.refer_edit);
        latEditView = (TextView) view.findViewById(R.id.latitude_edit);
        lngEditView = (TextView) view.findViewById(R.id.longitude_edit);
        areaEditView = (TextView) view.findViewById(R.id.area_edit);

        latSelectView = (TextView) view.findViewById(R.id.latitude_select);
        lngSelectView = (TextView) view.findViewById(R.id.longitude_select);

        titleView = (TextView) view.findViewById(R.id.title);
        detailView = (TextView) view.findViewById(R.id.detail);
        referView = (TextView) view.findViewById(R.id.refer);
        areaView = (TextView) view.findViewById(R.id.area);
        typeView = (TextView) view.findViewById(R.id.type);
        latitudeView = (TextView) view.findViewById(R.id.latitude);
        longitudeView = (TextView) view.findViewById(R.id.longitude);

        imageView = (ImageView) view.findViewById(R.id.image);
        thumbnailView = (ImageView) view.findViewById(R.id.thumbnail);

        resultIconView = (ImageView) view.findViewById(R.id.result_icon);
        resultTextView = (TextView) view.findViewById(R.id.result_text);
        homeBtn = view.findViewById(R.id.home_btn);
        backBtn = view.findViewById(R.id.back_btn);
        backFormBtn = view.findViewById(R.id.back_form_btn);
        backGeoBtn = view.findViewById(R.id.back_geo_btn);
        saveGeoBtn = view.findViewById(R.id.save_geo_btn);

        eventTypeValues = getResources().getStringArray(R.array.event_type_value);

        SupportMapFragment fragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_layout, fragment);
        fragmentTransaction.commit();
        fragment.getMapAsync(this);
        
        int pos = 0;

        for (int eventBtn : eventBtns) {
            if (getCctvActivity().getEventType() == Integer.parseInt(eventTypeValues[pos])) {
                ((ImageView) view.findViewById(eventBtn)).setImageResource(eventSelectedImages[pos]);
            } else {
                ((ImageView) view.findViewById(eventBtn)).setImageResource(eventImages[pos]);
            }

            view.findViewById(eventBtn).setOnClickListener(new EventTypeOnClickListener(eventTypeValues[pos++]));
        }

        new ListTask().execute(String.valueOf(getCctvActivity().getEventType()));

        // Check user if null then remove add button
        //
        if (getApp().getUser() == null) {
            view.findViewById(R.id.add_btn).setVisibility(View.GONE);
        } else {
            geoLayout.setVisibility(View.VISIBLE);

            view.findViewById(R.id.add_btn).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    eventType = "";
                    roadId = "0";

                    titleEditView.setText("");
                    detailEditView.setText("");
                    referEditView.setText("");
                    latEditView.setText("");
                    lngEditView.setText("");
                    latSelectView.setText("");
                    lngSelectView.setText("");
                    areaEditView.setText(R.string.lbl_default_area);
                    eventTypeView.setText(R.string.lbl_default_type);
                    thumbnailView.setImageResource(R.drawable.no_avatar);
                    photoBitmap = null;

                    mediaBtn.setImageResource(R.drawable.ic_add_image_empty);
                    geoBtn.setImageResource(R.drawable.ic_add_geo_empty);

                    // Get current location
                    //
                    Location location = getCctvActivity().getLocation();

                    if (location != null) {
                        latSelectView.setText(String.valueOf(location.getLatitude()));
                        lngSelectView.setText(String.valueOf(location.getLongitude()));
                        latEditView.setText(String.valueOf(location.getLatitude()));
                        lngEditView.setText(String.valueOf(location.getLongitude()));

                        geoBtn.setImageResource(R.drawable.ic_add_geo);
                    }

                    listLayout.setVisibility(View.GONE);
                    addLayout.setVisibility(View.VISIBLE);
                }
            });

            geoBtn.setOnClickListener(new GeoSelectOnClickListener());
            latEditView.setOnClickListener(new GeoSelectOnClickListener());
            lngEditView.setOnClickListener(new GeoSelectOnClickListener());

            view.findViewById(R.id.cancel_btn).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    listLayout.setVisibility(View.VISIBLE);
                    addLayout.setVisibility(View.GONE);
                }
            });

            final String[] titles = getResources().getStringArray(R.array.event_add_type_title);
            final String[] values = getResources().getStringArray(R.array.event_add_type_value);

            eventTypeView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Builder builder = new Builder(getActivity());
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_list_item_1);
                    arrayAdapter.addAll(titles);
                    builder.setAdapter(arrayAdapter, new Dialog.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            eventTypeView.setText(titles[which]);
                            eventType = values[which];
                        }
                    });

                    builder.create().show();
                }
            });

            mediaBtn.setOnClickListener(new MediaSelectOnClickListener());
            thumbnailView.setOnClickListener(new MediaSelectOnClickListener());

            dataNotFoundView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    new ListTask().execute(String.valueOf(getCctvActivity().getEventType()));
                }
            });

            areaBtn.setOnClickListener(new AreaSelectOnClickListener());
            areaEditView.setOnClickListener(new AreaSelectOnClickListener());

            view.findViewById(R.id.review_btn).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (titleEditView.getText().toString().isEmpty()) {
                        titleEditView.requestFocus();
                        Toast.makeText(getActivity(), R.string.dialog_validate_title, Toast.LENGTH_LONG).show();
                    } else if (detailEditView.getText().toString().isEmpty()) {
                        detailEditView.requestFocus();
                        Toast.makeText(getActivity(), R.string.dialog_validate_detail, Toast.LENGTH_LONG).show();
                    } else if (referEditView.getText().toString().isEmpty()) {
                        referEditView.requestFocus();
                        Toast.makeText(getActivity(), R.string.dialog_validate_reference, Toast.LENGTH_LONG).show();
                    } else if (eventTypeView.getText().equals(getString(R.string.lbl_default_type))) {
                        Toast.makeText(getActivity(), R.string.dialog_validate_event, Toast.LENGTH_LONG).show();
                    } else if (photoBitmap == null) {
                        Toast.makeText(getActivity(), R.string.dialog_validate_photo, Toast.LENGTH_LONG).show();
                    } else if (latEditView.getText().toString().isEmpty() || lngEditView.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "Please select location", Toast.LENGTH_LONG).show();
                    } else {
                        addLayout.setVisibility(View.GONE);
                        reviewLayout.setVisibility(View.VISIBLE);

                        titleView.setText(titleEditView.getText().toString());
                        detailView.setText(detailEditView.getText().toString());
                        referView.setText(referEditView.getText().toString());
                        areaView.setText(areaEditView.getText().toString());
                        typeView.setText(eventTypeView.getText().toString());
                        latitudeView.setText(String.valueOf(latitude));
                        longitudeView.setText(String.valueOf(longitude));
                        imageView.setImageBitmap(photoBitmap);
                    }
                }
            });

            view.findViewById(R.id.submit_btn).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    new EventCreateTask().execute();
                }
            });

            homeBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    resultLayout.setVisibility(View.GONE);
                    listLayout.setVisibility(View.VISIBLE);

                    new ListTask().execute(String.valueOf(getCctvActivity().getEventType()));
                }
            });

            backBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    resultLayout.setVisibility(View.GONE);
                    addLayout.setVisibility(View.VISIBLE);
                }
            });

            backFormBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    reviewLayout.setVisibility(View.GONE);
                    addLayout.setVisibility(View.VISIBLE);
                }
            });

            backGeoBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    geoLayout.setVisibility(View.GONE);
                    addLayout.setVisibility(View.VISIBLE);
                }
            });

            saveGeoBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    geoBtn.setImageResource(R.drawable.ic_add_geo);
                    geoLayout.setVisibility(View.GONE);
                    addLayout.setVisibility(View.VISIBLE);
                    latitude = Double.parseDouble(latSelectView.getText().toString());
                    longitude = Double.parseDouble(lngSelectView.getText().toString());
                    latEditView.setText(String.valueOf(latitude));
                    lngEditView.setText(String.valueOf(longitude));
                }
            });

            List<Area> areas = new ArrayList<Area>();
            Area defaultArea = new Area();
            defaultArea.setId("0");
            defaultArea.setName(getString(R.string.lbl_default_area));
            areas.add(defaultArea);
            areas.addAll(getApp().getAreas());

            pos = 0;
            final boolean isEng = getApp().getLocale() == Locale.ENGLISH;
            
            for (final Area area : areas) {
                View row = getActivity().getLayoutInflater().inflate(R.layout.area_item, areasLayout, false);
                TextView titleView = (TextView) row.findViewById(R.id.title);

                titleView.setText(isEng ? area.getNameEn() : area.getName());

                if (pos++ % 2 != 0) {
                    row.setBackgroundResource(R.color.grey);
                }

                row.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
                        roadId = area.getId();
                        areaEditView.setText(isEng ? area.getNameEn() : area.getName());
                        addLayout.setVisibility(View.VISIBLE);
                        areaListLayout.setVisibility(View.GONE);
                        areaBtn.setImageResource(R.drawable.ic_add_road);
                    }
                });

                areasLayout.addView(row);
            }
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_CAMERA) {
                try {
                    photoBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri);
                    mediaBtn.setImageResource(R.drawable.ic_add_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (photoBitmap != null) {
                    photoBitmap = ImageHelper.scaleBitmap(photoBitmap, IMAGE_SIZE);
                    thumbnailView.setImageBitmap(photoBitmap);
                } else {
                    thumbnailView.setImageResource(R.drawable.no_avatar);
                }
            } else if (requestCode == REQ_GALLERY) {
                try {
                    photoBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    mediaBtn.setImageResource(R.drawable.ic_add_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (photoBitmap != null) {
                    photoBitmap = ImageHelper.scaleBitmap(photoBitmap, IMAGE_SIZE);
                    thumbnailView.setImageBitmap(photoBitmap);
                } else {
                    thumbnailView.setImageResource(R.drawable.no_avatar);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        currentMarker = new MarkerOptions()
        .position(new LatLng(latitude, longitude))
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
        .draggable(false);
        
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        map.addMarker(currentMarker);
        
        map.setOnMapClickListener(new OnMapClickListener() {
            
            @Override
            public void onMapClick(LatLng point) {
                latSelectView.setText(String.valueOf(point.latitude));
                lngSelectView.setText(String.valueOf(point.longitude));
                
                currentMarker = new MarkerOptions()
                .position(point)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
                .draggable(false);
                
                map.clear();
                map.addMarker(currentMarker);
                map.animateCamera(CameraUpdateFactory.newLatLng(point));
            }
        });
    }
    
    /**
     * Create temporary file for camera shoot
     * 
     * @return Temporary file
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        return File.createTempFile(imageFileName, ".jpg",
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
    }

    public View getListLayout() {
        return listLayout;
    }

    public View getAddLayout() {
        return addLayout;
    }

    public View getReviewLayout() {
        return reviewLayout;
    }

    public LinearLayout getEventsLayout() {
        return eventsLayout;
    }

    public View getAreaListLayout() {
        return areaListLayout;
    }

    private class ListTask extends AsyncTask<String, Void, List<Event>> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "", "Get information...");
        }

        @Override
        protected List<Event> doInBackground(String... params) {
            return SoapConnector.listEvents(params[0]);
        }

        @Override
        protected void onPostExecute(List<Event> result) {
            dialog.dismiss();
            eventsLayout.removeAllViews();

            int pos = 0;

            if (!result.isEmpty()) {
                listView.setVisibility(View.VISIBLE);
                dataNotFoundView.setVisibility(View.GONE);

                for (final Event event : result) {
                    View row = getActivity().getLayoutInflater().inflate(R.layout.info_item, null, false);
                    TextView titleView = (TextView) row.findViewById(R.id.title);
                    TextView detailView = (TextView) row.findViewById(R.id.description);
                    ImageView imageView = (ImageView) row.findViewById(R.id.thumbnail);

                    if (event.getHeadline().length() > 50) {
                        titleView.setText(event.getHeadline().substring(0, 50) + "...");
                    } else {
                        titleView.setText(event.getHeadline());
                    }

                    if (event.getDetail().length() > 100) {
                        detailView.setText(event.getDetail().substring(0, 100) + "...");
                    } else {
                        detailView.setText(event.getDetail());
                    }

                    if (pos++ % 2 == 0) {
                        row.setBackgroundResource(R.color.white);
                    } else {
                        row.setBackgroundResource(R.color.grey);
                    }

                    row.setOnClickListener(new OnClickListener() {

                        public void onClick(View v) {
                            getCctvActivity().openEventDetail(event.getId());
                        }
                    });

                    new GetThumbnailTask().executeOnExecutor(THREAD_POOL_EXECUTOR, event.getImagePath(), imageView);

                    eventsLayout.addView(row);
                }
            } else {
                listView.setVisibility(View.GONE);
                dataNotFoundView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class GetThumbnailTask extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imageView;

        @Override
        protected Bitmap doInBackground(Object... params) {
            imageView = (ImageView) params[1];

            try {
                URL url = ConnectionHelper.encodeUrlWithThaiChar((String) params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                return BitmapFactory.decodeStream(connection.getInputStream());
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                imageView.setImageBitmap(ImageHelper.scaleBitmap(result, THUMBNAIL_SIZE));
            }
        }
    }

    private class EventCreateTask extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(getActivity(), "Creating event", "Sending event information...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Calendar calStart = Calendar.getInstance();
            Calendar calStop = Calendar.getInstance();
            calStop.add(Calendar.DATE, 1);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            byte[] image = stream.toByteArray();
            String encodedImage = Base64.encodeToString(image, Base64.DEFAULT);

            Event event = new Event();
            event.setType(Integer.parseInt(eventType));
            event.setRoadId(roadId);
            event.setHeadline(titleEditView.getText().toString());
            event.setDetail(detailEditView.getText().toString());
            event.setRefer(referEditView.getText().toString());
            event.setCreateEventPhoto(encodedImage);
            event.setLatitude(latitude);
            event.setLongitude(longitude);
            event.setDateStart(calStart.getTime());
            event.setDateStop(calStop.getTime());

            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return SoapConnector.createEvent(event, getApp().getUser());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();

            reviewLayout.setVisibility(View.GONE);
            resultLayout.setVisibility(View.VISIBLE);

            if (result != null && result) {
                resultIconView.setImageResource(R.drawable.ic_correct);
                resultTextView.setText(R.string.lbl_event_create_succeed);
                homeBtn.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
            } else {
                resultIconView.setImageResource(R.drawable.ic_error);
                resultTextView.setText(R.string.lbl_event_create_failed);
                homeBtn.setVisibility(View.GONE);
                backBtn.setVisibility(View.VISIBLE);
            }
        }
    }

    private class EventTypeOnClickListener implements OnClickListener {

        private String type;

        public EventTypeOnClickListener(String type) {
            this.type = type;
        }

        public void onClick(View v) {
            int pos = 0;

            for (int eventBtn : eventBtns) {
                if (v.getId() == eventBtn) {
                    ((ImageView) view.findViewById(eventBtn)).setImageResource(eventSelectedImages[pos++]);
                } else {
                    ((ImageView) view.findViewById(eventBtn)).setImageResource(eventImages[pos++]);
                }
            }

            getCctvActivity().setEventType(Integer.parseInt(type));
            new ListTask().execute(type);
        }
    }

    private class MediaSelectOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            Builder builder = new Builder(getActivity());
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1);
            arrayAdapter.add("Take photo");
            arrayAdapter.add("Choose photo");
            builder.setAdapter(arrayAdapter, new Dialog.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        try {
                            photoUri = Uri.fromFile(createImageFile());
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            startActivityForResult(intent, REQ_CAMERA);
                        } catch (Exception e) {

                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK);

                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                            intent.setType("image/*");
                            startActivityForResult(intent, REQ_GALLERY);
                        }
                    }
                }
            });

            builder.create().show();
        }
    }

    private class GeoSelectOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            addLayout.setVisibility(View.GONE);
            geoLayout.setVisibility(View.VISIBLE);
            
            latSelectView.setText(String.valueOf(latitude));
            lngSelectView.setText(String.valueOf(longitude));
            
            currentMarker = new MarkerOptions()
            .position(new LatLng(latitude, longitude))
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
            .draggable(false);
            
            map.clear();
            map.addMarker(currentMarker);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
        }
    }

    private class AreaSelectOnClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            addLayout.setVisibility(View.GONE);
            areaListLayout.setVisibility(View.VISIBLE);
        }
    }
}
