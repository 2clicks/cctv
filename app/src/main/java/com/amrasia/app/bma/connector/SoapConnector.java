package com.amrasia.app.bma.connector;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.annotation.SuppressLint;
import android.util.Log;

import com.amrasia.app.bma.model.Area;
import com.amrasia.app.bma.model.Cctv;
import com.amrasia.app.bma.model.Event;
import com.amrasia.app.bma.model.News;
import com.amrasia.app.bma.model.User;
import com.amrasia.app.bma.model.Zone;
import com.google.android.gms.maps.model.LatLng;

/**
 * 
 * @author BabeDev
 * 
 */
public class SoapConnector {

    protected static final String TAG = SoapConnector.class.getSimpleName();

    private static final String ENDPOINT = "http://110.170.214.36/cctvservice/SerViceCCTV.asmx?WSDL";
    private static final String NAMESPACE = "http://tempuri.org/";
    
    public static final int CCTV_TYPE_CCTV = 1;
    public static final int CCTV_TYPE_NEWS = 2;
    public static final int CCTV_TYPE_EVENT = 3;

    public static List<News> listNews() {
        List<News> list = new ArrayList<News>();

        SoapObject response = (SoapObject) request("http://tempuri.org/public_list", new SoapObject(NAMESPACE,
                "public_list"));

        if (response != null) {
            try {
                SoapObject items = (SoapObject) response.getProperty("tbl_news_list");

                for (int i = 0; i < items.getPropertyCount(); ++i) {
                    SoapObject item = (SoapObject) items.getProperty(i);
                    News news = new News();
                    news.setId(item.getPropertyAsString("public_relation_id"));
                    news.setHeadline(item.getPropertyAsString("headline"));
                    news.setDetail(item.getPropertyAsString("detail"));
                    news.setImagePath(item.getPropertyAsString("image_message"));
                    list.add(news);
                }
            } catch (Exception e) {
                Log.w(TAG, "Cannot get news");
            }
        }

        return list;
    }

    public static News getNews(String id) {
        SoapObject request = new SoapObject(NAMESPACE, "public_detail");
        PropertyInfo property = new PropertyInfo();
        property.setNamespace(NAMESPACE);
        property.setName("public_relation_id");
        property.setValue(id);
        request.addProperty(property);

        SoapObject response = (SoapObject) request("http://tempuri.org/public_detail", request);

        if (response != null) {
            SoapObject items = (SoapObject) response.getProperty("tbl_news_detail");
            SoapObject item = (SoapObject) items.getProperty(0);

            News news = new News();
            news.setHeadline(item.getPropertyAsString("headline"));
            news.setDetail(item.getPropertyAsString("detail"));
            news.setImagePath(item.getPropertyAsString("image_message"));
            news.setRefer(item.getPropertyAsString("data_refer"));
            news.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
            news.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));

            return news;
        }

        return null;
    }

    public static List<Event> listEvents(String id) {
        List<Event> list = new ArrayList<Event>();

        SoapObject request = new SoapObject(NAMESPACE, "event_list");
        PropertyInfo property = new PropertyInfo();
        property.setNamespace(NAMESPACE);
        property.setName("event_type_id");
        property.setValue(id);
        request.addProperty(property);

        SoapObject response = (SoapObject) request("http://tempuri.org/event_list", request);

        if (response != null) {
            try {
                SoapObject items = (SoapObject) response.getProperty("tbl_event_list");

                for (int i = 0; i < items.getPropertyCount(); ++i) {
                    SoapObject item = (SoapObject) items.getProperty(i);
                    Event event = new Event();
                    event.setId(item.getPropertyAsString("event_id"));
                    event.setHeadline(item.getPropertyAsString("headline"));
                    event.setDetail(item.getPropertyAsString("detail"));
                    event.setImagePath(item.getPropertyAsString("image_message"));
                    list.add(event);
                }
            } catch (Exception e) {
                Log.w(TAG, "Cannot get events");
            }
        }

        return list;
    }

    public static Event getEvent(String id) {
        SoapObject request = new SoapObject(NAMESPACE, "event_detail");
        PropertyInfo property = new PropertyInfo();
        property.setNamespace(NAMESPACE);
        property.setName("event_id");
        property.setValue(id);
        request.addProperty(property);

        SoapObject response = (SoapObject) request("http://tempuri.org/event_detail", request);

        if (response != null) {
            SoapObject items = (SoapObject) response.getProperty("tbl_event_detail");
            SoapObject item = (SoapObject) items.getProperty(0);

            Event event = new Event();
            event.setHeadline(item.getPropertyAsString("headline"));
            event.setDetail(item.getPropertyAsString("detail"));
            event.setImagePath(item.getPropertyAsString("image_message"));
            event.setRefer(item.getPropertyAsString("data_refer"));
            event.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
            event.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));
            event.setType(Integer.parseInt(item.getPropertyAsString("event_type_id")));

            return event;
        }

        return null;
    }

    public static List<Area> listAreas() {
        List<Area> list = new ArrayList<Area>();

        SoapObject response = (SoapObject) request("http://tempuri.org/road_list", new SoapObject(NAMESPACE, "road_list"));

        if (response != null) {
            try {
                SoapObject items = (SoapObject) response.getProperty("tbl_road");

                for (int i = 0; i < items.getPropertyCount(); ++i) {
                    SoapObject item = (SoapObject) items.getProperty(i);
                    Area area = new Area();
                    area.setId(item.getPropertyAsString("road_id"));
                    area.setName(item.getPropertyAsString("name"));
                    area.setNameEn(item.getPropertyAsString("name_en"));
                    list.add(area);
                }
            } catch (Exception e) {
                Log.w(TAG, "Cannot get areas");
            }
        }

        return list;
    }

    public static List<Zone> listZones() {
        List<Zone> list = new ArrayList<Zone>();

        SoapObject response = (SoapObject) request("http://tempuri.org/Camera_list", new SoapObject(NAMESPACE, "Camera_list"));

        if (response != null) {
            try {
                SoapObject items = (SoapObject) response.getProperty("tbl_camera_preview");

                for (int i = 0; i < items.getPropertyCount(); ++i) {
                    SoapObject item = (SoapObject) items.getProperty(i);
                    Zone zone = new Zone();
                    zone.setId(item.getPropertyAsString("camera_preview_id"));
                    zone.setName(item.getPropertyAsString("location"));
                    zone.setNameEn(item.getPropertyAsString("location_en"));
                    zone.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
                    zone.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));
                    list.add(zone);
                }
            } catch (Exception e) {
                Log.w(TAG, "Cannot get zones");
            }
        }

        return list;
    }
    
    public static List<Cctv> listCctv(int type) {
        List<Cctv> list = new ArrayList<Cctv>();

        SoapObject response = null;
        
        if (type == CCTV_TYPE_CCTV) {
        	response = (SoapObject) request("http://tempuri.org/map_camera", new SoapObject(NAMESPACE, "map_camera"));
        } else if (type == CCTV_TYPE_NEWS) {
        	response = (SoapObject) request("http://tempuri.org/map_public", new SoapObject(NAMESPACE, "map_public"));
        } else if (type == CCTV_TYPE_EVENT) {
        	response = (SoapObject) request("http://tempuri.org/map_event", new SoapObject(NAMESPACE, "map_event"));
        }
        
        if (response != null) {
            try {
            	if (type == CCTV_TYPE_CCTV) {
	                SoapObject items = (SoapObject) response.getProperty("tbl_camera_preview");
	
	                for (int i = 0; i < items.getPropertyCount(); ++i) {
	                    SoapObject item = (SoapObject) items.getProperty(i);
	                    Cctv cctv = new Cctv();
	                    cctv.setId(item.getPropertyAsString("camera_preview_id"));
	                    cctv.setIp(item.getPropertyAsString("ip_camera"));
	                    cctv.setLocation(item.getPropertyAsString("location"));
	                    cctv.setLocationEn(item.getPropertyAsString("location_en"));
	                    cctv.setDescription(item.getPropertyAsString("discription_public"));
	                    cctv.setDescriptionEn(item.hasProperty("discription_public_en") ? item.getPropertyAsString("discription_public_en") : "-");
	                    cctv.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
	                    cctv.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));
	                    list.add(cctv);
	                } 
            	} else if (type == CCTV_TYPE_EVENT) {
            		SoapObject items = (SoapObject) response.getProperty("tbl_event");
            		
	                for (int i = 0; i < items.getPropertyCount(); ++i) {
	                    SoapObject item = (SoapObject) items.getProperty(i);
	                    Cctv cctv = new Cctv();
	                    cctv.setId(item.getPropertyAsString("event_id"));
	                    cctv.setHeadline(item.getPropertyAsString("headline"));
	                    cctv.setDetail(item.getPropertyAsString("detail"));
	                    cctv.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
	                    cctv.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));
	                    cctv.setImageURL(item.getPropertyAsString("image_message"));
	                    cctv.setReference(item.getPropertyAsString("data_refer"));
	                    cctv.setTypeId(Integer.parseInt(item.getPropertyAsString("event_type_id")));
	                    list.add(cctv);
	                } 
            	} else if (type == CCTV_TYPE_NEWS) {
            		SoapObject items = (SoapObject) response.getProperty("tbl_public_relation");
            		
	                for (int i = 0; i < items.getPropertyCount(); ++i) {
	                    SoapObject item = (SoapObject) items.getProperty(i);
	                    Cctv cctv = new Cctv();
	                    cctv.setId(item.getPropertyAsString("public_relation_id"));
	                    cctv.setHeadline(item.getPropertyAsString("headline"));
	                    cctv.setDetail(item.getPropertyAsString("detail"));
	                    cctv.setLatitude(Double.parseDouble(item.getPropertyAsString("latitude")));
	                    cctv.setLongitude(Double.parseDouble(item.getPropertyAsString("longitude")));
	                    cctv.setImageURL(item.getPropertyAsString("image_message"));
	                    cctv.setReference(item.getPropertyAsString("data_refer"));
	                    list.add(cctv);
	                } 
            	}
            } catch (Exception e) {
                Log.w(TAG, "Cannot get cctv type " + type);
            }
        }

        return list;
    }

    @SuppressLint("SimpleDateFormat")
    public static boolean registerMember(User user) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SoapObject request = new SoapObject(NAMESPACE, "member_createV2");
        request.addProperty(createPropertyInfo("username", user.getName()));
        request.addProperty(createPropertyInfo("fname", user.getFirstName()));
        request.addProperty(createPropertyInfo("lname", user.getLastName()));
        request.addProperty(createPropertyInfo("personal_code", user.getPid()));
        request.addProperty(createPropertyInfo("career", user.getCareer()));
        request.addProperty(createPropertyInfo("birthday", sdf.format(user.getDob())));
        request.addProperty(createPropertyInfo("email", user.getEmail()));
        request.addProperty(createPropertyInfo("password", user.getPassword()));
        request.addProperty(createPropertyInfo("sex_id", user.getGender()));
        request.addProperty(createPropertyInfo("road_id", user.getRoads()));
        request.addProperty(createPropertyInfo("type_news_id", user.getNewsType()));
        request.addProperty(createPropertyInfo("register_from", "2"));
        request.addProperty(createPropertyInfo("image_file", user.getProfileImage()));

        SoapPrimitive response = (SoapPrimitive) request("http://tempuri.org/member_createV2", request);

        return response != null && ((String) response.getValue()).equalsIgnoreCase("1");
    }

    @SuppressLint("SimpleDateFormat")
    public static boolean updateMember(User user) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SoapObject request = new SoapObject(NAMESPACE, "member_edit");
        request.addProperty(createPropertyInfo("member_id", user.getId()));
        request.addProperty(createPropertyInfo("fname", user.getFirstName()));
        request.addProperty(createPropertyInfo("lname", user.getLastName()));
        request.addProperty(createPropertyInfo("personal_code", user.getPid()));
        request.addProperty(createPropertyInfo("career", user.getCareer()));
        request.addProperty(createPropertyInfo("birthday", sdf.format(user.getDob())));
        request.addProperty(createPropertyInfo("email", user.getEmail()));
        request.addProperty(createPropertyInfo("password", user.getPassword()));
        request.addProperty(createPropertyInfo("sex_id", user.getGender()));
        request.addProperty(createPropertyInfo("road_id", user.getRoads()));
        request.addProperty(createPropertyInfo("image_file", user.getProfileImage()));
        request.addProperty(createPropertyInfo("type_news_id", user.getNewsType()));

        SoapPrimitive response = (SoapPrimitive) request("http://tempuri.org/member_edit", request);

        return response != null && ((String) response.getValue()).equalsIgnoreCase("1");
    }

    public static User login(String username, String password) {
        SoapObject request = new SoapObject(NAMESPACE, "member_detail");
        request.addProperty(createPropertyInfo("username", username));
        request.addProperty(createPropertyInfo("password", password));

        SoapObject response = (SoapObject) request("http://tempuri.org/member_detail", request);

        if (response != null) {
            try {
                SoapObject items = (SoapObject) response.getProperty("tbl_member");

                if (items.getPropertyCount() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    SoapObject item = (SoapObject) items.getProperty(0);

                    User user = new User();
                    user.setName(username);
                    user.setId(item.getPropertyAsString("member_id"));
                    user.setPassword(item.getPropertyAsString("password"));
                    user.setCareer(item.getPropertyAsString("career"));
                    user.setPid(item.getPropertyAsString("identification_number"));
                    user.setFirstName(item.getPropertyAsString("first_name"));
                    user.setLastName(item.getPropertyAsString("last_name"));
                    user.setEmail(item.getPropertyAsString("email"));
                    user.setGender(item.getPropertyAsString("sex_id"));
                    user.setNewsType(item.getPropertyAsString("type_news_id"));
                    user.setRoads(item.getPropertyAsString("road_id"));
                    user.setDob(sdf.parse(item.getPropertyAsString("birthday")));
                    user.setProfileImage(item.getPropertyAsString("image_profile"));
                    return user;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @SuppressLint("SimpleDateFormat")
    public static boolean forgetPassword(String username) {
        SoapObject request = new SoapObject(NAMESPACE, "public_forgotPassword");
        request.addProperty(createPropertyInfo("Username", username));

        SoapPrimitive response = (SoapPrimitive) request("http://tempuri.org/public_forgotPassword", request);

        return response != null && ((String) response.getValue()).equalsIgnoreCase("1");
    }

    public static boolean createEvent(Event event, User user) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SoapObject request = new SoapObject(NAMESPACE, "event_create");
        request.addProperty(createPropertyInfo("headline", event.getHeadline()));
        request.addProperty(createPropertyInfo("event_type_id", String.valueOf(event.getType())));
        request.addProperty(createPropertyInfo("detail", event.getDetail()));
        request.addProperty(createPropertyInfo("image", event.getCreateEventPhoto()));
        request.addProperty(createPropertyInfo("data_refer", event.getRefer()));
        request.addProperty(createPropertyInfo("road_id", event.getRoadId()));
        request.addProperty(createPropertyInfo("latitude", String.valueOf(event.getLatitude())));
        request.addProperty(createPropertyInfo("longitude", String.valueOf(event.getLongitude())));
        request.addProperty(createPropertyInfo("date_start", sdf.format(event.getDateStart())));
        request.addProperty(createPropertyInfo("date_stop", sdf.format(event.getDateStop())));
        request.addProperty(createPropertyInfo("member_id", user.getId()));

        SoapPrimitive response = (SoapPrimitive) request("http://tempuri.org/event_create", request);

        return response != null && ((String) response.getValue()).equalsIgnoreCase("1");
    }

    public static List<LatLng> searchPlace(String keyword, double latitude, double longitude, String range,
            boolean withLocation) throws JSONException {
        SoapObject request = new SoapObject(NAMESPACE, "public_searchESRI");

        if (withLocation) {
            request.addProperty(createPropertyInfo("Keyword", keyword + ",True," + latitude + "," + longitude + ","
                    + range));
        } else {
            request.addProperty(createPropertyInfo("Keyword", keyword + ",False"));
        }

        SoapPrimitive response = (SoapPrimitive) request("http://tempuri.org/public_searchESRI", request);
        JSONArray places = new JSONObject(response.getValue().toString()).getJSONObject("response")
                .getJSONArray("docs");

        List<LatLng> latLngs = new ArrayList<LatLng>();

        for (int i = 0; i < places.length(); ++i) {
            JSONObject place = places.getJSONObject(i);
            String[] geo = place.getString("latlon").split(",");
            latLngs.add(new LatLng(Double.parseDouble(geo[0]), Double.parseDouble(geo[1])));
        }

        return latLngs;
    }

    private static PropertyInfo createPropertyInfo(String name, String value) {
        PropertyInfo property = new PropertyInfo();
        property.setNamespace(NAMESPACE);
        property.setName(name);
        property.setValue(value);
        return property;
    }

    private static Object request(String action, SoapObject request) {
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.encodingStyle = SoapEnvelope.ENC;
        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransport = new HttpTransportSE(ENDPOINT);
        httpTransport.debug = true;
        try {
            httpTransport.call(action, envelope);
//                         Log.d(TAG, "=============== Request");
//                         Log.d(TAG, httpTransport.requestDump);
//                         Log.d(TAG, "=============== Response");
//                         Log.d(TAG, httpTransport.responseDump);
            return envelope.getResponse();
        } catch (Exception e) {
            Log.e(TAG, "Cannot request web service: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}
