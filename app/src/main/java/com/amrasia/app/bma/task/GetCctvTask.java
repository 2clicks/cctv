package com.amrasia.app.bma.task;

import java.net.URLEncoder;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Cctv;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * 
 * @author BabeDev
 *
 */
public class GetCctvTask extends AsyncTask<Object, Void, List<Cctv>> {

    public static final int CCTV_TYPE_CCTV = 1;
    public static final int CCTV_TYPE_NEWS = 2;
    public static final int CCTV_TYPE_EVENT = 3;

    private Context context;
    private GoogleMap map;

    private boolean isAllPoi; // If true then show all POI
    private String id;
    private int type;
    private int eventType;

    public GetCctvTask(Context context) {
        this.context = context;
    }

    public GetCctvTask(Context context, GoogleMap map) {
        this.context = context;
        this.map = map;
    }

    public GetCctvTask(Context context, GoogleMap map, boolean isAllPOi) {
        this.context = context;
        this.map = map;
        this.isAllPoi = isAllPOi;
    }

    @Override
    protected List<Cctv> doInBackground(Object... params) {
        type = (Integer) params[0];

        if (params.length > 2) {
            eventType = (Integer) params[1];
            id = (String) params[2];
        } else if (params.length > 1) {
            id = (String) params[1];
        }

        return SoapConnector.listCctv(type);
    }

    @Override
    protected void onPostExecute(List<Cctv> result) {
        if (map != null) {
            if (!isAllPoi) {
                map.clear();
            }

            for (Cctv cctv : result) {
                if (type == SoapConnector.CCTV_TYPE_CCTV) {
                    map.addMarker(new MarkerOptions()
                            .position(new LatLng(cctv.getLatitude(), cctv.getLongitude()))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_camera_cctv))
                            .draggable(false)
                            .title(SoapConnector.CCTV_TYPE_CCTV + ";" + cctv.getId() + ";" + cctv.getLocation() + ";"
                                    + cctv.getLocationEn() + ";" + cctv.getIp() + ";" + cctv.getDescription() + ";" + cctv.getDescriptionEn()));
                } else if (type == SoapConnector.CCTV_TYPE_EVENT) {
                    if (eventType == 0 || eventType == cctv.getTypeId()) {
                        int resId = R.drawable.ic_camera_event_hit;

                        switch (cctv.getTypeId()) {
                        case 2:
                            resId = R.drawable.ic_camera_event_mob;
                            break;
                        case 3:
                            resId = R.drawable.ic_camera_event_fix;
                            break;
                        case 4:
                            resId = R.drawable.ic_camera_event_road;
                            break;
                        case 5:
                            resId = R.drawable.ic_camera_event_traffic;
                            break;
                        case 6:
                            resId = R.drawable.ic_camera_event_construct;
                            break;
                        case 7:
                            resId = R.drawable.ic_camera_event_repair;
                            break;
                        case 8:
                            resId = R.drawable.ic_camera_event_fire;
                            break;
                        case 9:
                            resId = R.drawable.ic_camera_event_rain;
                            break;
                        }

                        if (id == null || id.isEmpty() || (id != null && !id.isEmpty() && id.equals(cctv.getId()))) {
                            Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(cctv.getLatitude(), cctv.getLongitude()))
                                    .icon(BitmapDescriptorFactory.fromResource(resId))
                                    .draggable(false)
                                    .title(SoapConnector.CCTV_TYPE_EVENT + ";" + cctv.getId() + ";" + cctv.getHeadline()
                                            + ";" + cctv.getDetail() + ";" + encodeURL(cctv.getImageURL())));
    
                            if (id != null && !id.isEmpty() && id.equals(cctv.getId())) {
                                marker.showInfoWindow();
                            }
                        }
                    }
                } else if (type == SoapConnector.CCTV_TYPE_NEWS) {
                    if (id == null || id.isEmpty() || (id != null && !id.isEmpty() && id.equals(cctv.getId()))) {
                        Marker marker = map.addMarker(new MarkerOptions()
                                .position(new LatLng(cctv.getLatitude(), cctv.getLongitude()))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_camera_info))
                                .draggable(false)
                                .title(SoapConnector.CCTV_TYPE_NEWS + ";" + cctv.getId() + ";" + cctv.getHeadline() + ";"
                                        + cctv.getDetail() + ";" + encodeURL(cctv.getImageURL())));
                        
                        if (id != null && !id.isEmpty() && id.equals(cctv.getId())) {
                            marker.showInfoWindow();
                        }
                    }
                }
            }
        }
    }
    
    private String encodeURL(String path) {
        // Since API send a stupid data and they will not correct it so I have to replace '//images' to '/images' by myself
        // using Thai language for file name.... How stupid they are
        //
        path = path.replaceAll("//images", "/images");
        
        try {
            path = URLEncoder.encode(path, "UTF-8");
            path = path.replaceAll("%3A", ":");
            path = path.replaceAll("%2F", "/");
            path = path.replace("+", "%20");
        } catch (Exception e) {
            // Do nothing
        }
        
        return path;
    }
}
