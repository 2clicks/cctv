package com.amrasia.app.bma.task;

/**
 * 
 * @author BabeDev
 *
 */
public abstract class GetCctvCallback {

	public abstract void process();
	
}
