package com.amrasia.app.bma.flow;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Event;
import com.amrasia.app.bma.model.News;
import com.amrasia.app.bma.model.Zone;
import com.amrasia.app.bma.task.GetCctvTask;
import com.amrasia.app.bma.utils.CctvConstant;
import com.amrasia.app.bma.utils.ConnectionHelper;
import com.amrasia.app.bma.utils.ImageHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author BabeDev
 * 
 */
public class MapFragment extends CctvFragment implements OnMapReadyCallback {

    private static final String TAG = MapFragment.class.getSimpleName();

    private static final String IMAGE_CCTV = "http://110.170.214.36/images/clips/";

    public static final int TYPE_INFO = 0;
    public static final int TYPE_EVENT = 1;
    public static final int TYPE_CCTV = 2;
    public static final int TYPE_TRAFFIC = 3;

    private View headerLayout;
    private ScrollView areaListLayout;
    private ScrollView eventListLayout;
    private LinearLayout areasLayout;
    private LinearLayout eventsLayout;
    private LinearLayout searchPlaceLayout;
    private RelativeLayout cctvLayout;
    private View fullDetailLayout;
    private View trafficInfoLayout;
    private WebView searchView;
    private WebView searchPlaceView;
    private TextView rangeSearchBtn;

    private TextView fullTitleView;
    private TextView fullDetailView;
    private TextView fullReferView;
    private ImageView fullImageView;

    private EditText placeView;
    private TextView cctvNameView;
    private TextView cctvDetailView;
    private ImageView cctvView;
    private ImageView directionView;

    private ImageView cctvBtn;
    private ImageView eventBtn;
    private ImageView infoBtn;
    private ImageView trafficBtn;
    private ImageView searchBtn;
    private ImageView searchPlaceBtn;
    private ImageView poiBtn;
    private ImageView cctvCloseBtn;

    private GetCctvPhotoTask getCctvPhotoTask;

    private String id;
    private int eventType;
    private double latitude;
    private double longitude;
    private int type = TYPE_CCTV;
    private int currentEventSeleted = R.drawable.ic_event_all_selected;
    private int currentEvent = R.drawable.ic_event_all;
    private String range = "0";
    private boolean isEng;
    private GoogleMap map;

    private Timer timer;

    private List<Marker> searchPlaceMarkers;
    private Marker currentMarker;

    public MapFragment() {
        searchPlaceMarkers = new ArrayList<Marker>();
    }

    public MapFragment(String id, double latitude, double longitude, int type, String title, String detail, Bitmap thumbnail) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
    }

    public MapFragment(String id, int eventType, double latitude, double longitude, int type, String title, String detail, Bitmap thumbnail) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.map, container, false);

        isEng = getApp().getLocale() == Locale.ENGLISH;
        
        headerLayout = view.findViewById(R.id.header);
        cctvView = (ImageView) view.findViewById(R.id.cctv);
        cctvCloseBtn = (ImageView) view.findViewById(R.id.cctv_close_btn);
        cctvNameView = (TextView) view.findViewById(R.id.cctv_name);
        cctvDetailView = (TextView) view.findViewById(R.id.cctv_detail);
        directionView = (ImageView) view.findViewById(R.id.direction);
        placeView = (EditText) view.findViewById(R.id.place);

        areaListLayout = (ScrollView) view.findViewById(R.id.area_list_layout);
        eventListLayout = (ScrollView) view.findViewById(R.id.event_list_layout);
        areasLayout = (LinearLayout) view.findViewById(R.id.area_layout);
        eventsLayout = (LinearLayout) view.findViewById(R.id.event_layout);
        cctvLayout = (RelativeLayout) view.findViewById(R.id.cctv_layout);
        searchPlaceLayout = (LinearLayout) view.findViewById(R.id.search_place_layout);
        trafficInfoLayout = view.findViewById(R.id.traffic_info_layout);
        fullDetailLayout = view.findViewById(R.id.fulldetail_layout);

        cctvBtn = (ImageView) view.findViewById(R.id.cctv_btn);
        eventBtn = (ImageView) view.findViewById(R.id.event_btn);
        infoBtn = (ImageView) view.findViewById(R.id.info_btn);
        trafficBtn = (ImageView) view.findViewById(R.id.traffic_btn);
        searchBtn = (ImageView) view.findViewById(R.id.search_btn);
        searchPlaceBtn = (ImageView) view.findViewById(R.id.search_place_btn);
        poiBtn = (ImageView) view.findViewById(R.id.poi);

        fullTitleView = (TextView) view.findViewById(R.id.fulltitle);
        fullDetailView = (TextView) view.findViewById(R.id.fulldetail);
        fullReferView = (TextView) view.findViewById(R.id.fullrefer);
        fullImageView = (ImageView) view.findViewById(R.id.fullimage);
        rangeSearchBtn = (TextView) view.findViewById(R.id.range_btn);

        SupportMapFragment fragment = SupportMapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_layout, fragment);
        fragmentTransaction.commit();
        fragment.getMapAsync(this);

        if (type == TYPE_INFO || type == TYPE_EVENT) {
            headerLayout.setVisibility(View.GONE);
        }
        
        cctvBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                map.setTrafficEnabled(false);
                refreshHeader();
                cctvBtn.setImageResource(R.drawable.ic_cctv_selected);

                type = TYPE_CCTV;

                new GetCctvTask(getActivity(), map).execute(GetCctvTask.CCTV_TYPE_CCTV);

                cctvLayout.setVisibility(View.GONE);
                areaListLayout.setVisibility(View.VISIBLE);
                eventListLayout.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.GONE);
            }
        });

        eventBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                map.setTrafficEnabled(false);
                type = TYPE_EVENT;
                refreshHeader();
                eventBtn.setImageResource(currentEventSeleted);

                cctvLayout.setVisibility(View.GONE);
                eventListLayout.setVisibility(View.VISIBLE);
                areaListLayout.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.GONE);
            }
        });

        infoBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                map.setTrafficEnabled(false);
                type = TYPE_INFO;
                refreshHeader();
                infoBtn.setImageResource(R.drawable.ic_info_selected);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(CctvConstant.DEFAULT_LATITUDE, CctvConstant.DEFAULT_LONGITUDE), 12));
                
                cctvLayout.setVisibility(View.GONE);
                eventListLayout.setVisibility(View.GONE);
                areaListLayout.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.GONE);

                new GetCctvTask(getActivity(), map).execute(GetCctvTask.CCTV_TYPE_NEWS);
            }
        });

        trafficBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                map.setTrafficEnabled(true);
                refreshHeader();
                closeInfoWindow();
                trafficBtn.setImageResource(R.drawable.ic_traffic_selected);

                type = TYPE_TRAFFIC;

                cctvLayout.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.VISIBLE);
                eventListLayout.setVisibility(View.GONE);
                areaListLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.GONE);
            }
        });

        searchBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                refreshHeader();
                searchBtn.setImageResource(R.drawable.ic_search_selected);
                searchView.reload();

                cctvLayout.setVisibility(View.GONE);
                areaListLayout.setVisibility(View.GONE);
                eventListLayout.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
            }
        });

        searchPlaceBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                map.setTrafficEnabled(false);
                map.clear();
                refreshHeader();
                searchPlaceBtn.setImageResource(R.drawable.ic_search_place_selected);
                searchPlaceView.loadUrl(isEng ? "http://27.254.44.58/~amr/places/en.html" : "http://27.254.44.58/~amr/places/th.html");
                
                cctvLayout.setVisibility(View.GONE);
                areaListLayout.setVisibility(View.GONE);
                eventListLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                trafficInfoLayout.setVisibility(View.GONE);
                searchPlaceView.setVisibility(View.VISIBLE);

                if (getCctvActivity().getLocation() != null) {
                    rangeSearchBtn.setVisibility(View.VISIBLE);
                } else {
                    rangeSearchBtn.setVisibility(View.GONE);
                }
            }
        });

        view.findViewById(R.id.search_place_submit_btn).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (placeView.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please input searching place", Toast.LENGTH_LONG).show();
                } else {
                    new SearchPlaceTask().execute();
                }
            }
        });

        view.findViewById(R.id.back).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                fullDetailLayout.setVisibility(View.GONE);
            }
        });

        poiBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                fullDetailLayout.setVisibility(View.GONE);
            }
        });

        view.findViewById(R.id.close_btn).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                trafficInfoLayout.setVisibility(View.GONE);
            }
        });

        final String[] rangeTitles = getResources().getStringArray(R.array.range_title);
        final String[] rangeValues = getResources().getStringArray(R.array.range_value);

        rangeSearchBtn.setText(rangeTitles[0]);
        rangeSearchBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Builder builder = new Builder(getActivity());
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_list_item_1);
                arrayAdapter.addAll(rangeTitles);
                builder.setAdapter(arrayAdapter, new Dialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        range = rangeValues[which];
                        rangeSearchBtn.setText(getString(R.string.lbl_range_title, rangeValues[which]));
                    }
                });

                builder.create().show();
            }
        });

        // Touch outer area to hide keyboard
        //
        searchPlaceLayout.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(placeView);
                return false;
            }
        });

        cctvCloseBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (timer != null) {
                    timer.cancel();
                }

                cctvLayout.setVisibility(View.GONE);
            }
        });

        searchView = (WebView) view.findViewById(R.id.search_view);
        WebSettings webSettings = searchView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setGeolocationEnabled(true);
        searchView.loadUrl(getString(R.string.search_path));
        searchView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, Callback callback) {
                callback.invoke(origin, true, false);
            }
        });
        
        searchPlaceView = (WebView) view.findViewById(R.id.search_place_view);
        WebSettings searchPlaceSettings = searchPlaceView.getSettings();
        searchPlaceSettings.setJavaScriptEnabled(true);
        searchPlaceView.loadUrl(isEng ? "http://27.254.44.58/~amr/places/en.html" : "http://27.254.44.58/~amr/places/th.html");
        
        /*
         * Adjust CCTV layout
         */
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        
        LayoutParams params = (LayoutParams) cctvLayout.getLayoutParams();
        params.width = width - ImageHelper.dp2px(getActivity(), 36) * 2;
        params.height = width / 3 * 2 + ImageHelper.dp2px(getActivity(), 36);
        cctvLayout.setLayoutParams(params);
        
        prepareZones();
        prepareEvents();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        //        mapView.pause();

        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (timer != null) {
            timer.cancel();
        }
    }

    public ScrollView getAreaListLayout() {
        return areaListLayout;
    }

    public View getFullDetailLayout() {
        return fullDetailLayout;
    }

    public LinearLayout getSearchPlaceLayout() {
        return searchPlaceLayout;
    }

    public ScrollView getEventListLayout() {
        return eventListLayout;
    }

    private void refreshHeader() {
        hideKeyboard(placeView);
        cctvBtn.setImageResource(R.drawable.ic_cctv);
        eventBtn.setImageResource(currentEvent);
        infoBtn.setImageResource(R.drawable.ic_info);
        trafficBtn.setImageResource(R.drawable.ic_traffic);
        searchBtn.setImageResource(R.drawable.ic_search);
        searchPlaceBtn.setImageResource(R.drawable.ic_search_place);
    }

    private void prepareZones() {
        List<Zone> list = getApp().getZones();

        int pos = 0;

        for (final Zone zone : list) {
            View row = getActivity().getLayoutInflater().inflate(R.layout.area_item, areasLayout, false);
            TextView titleView = (TextView) row.findViewById(R.id.title);
            ImageView imageView = (ImageView) row.findViewById(R.id.icon);

            titleView.setText(isEng ? zone.getNameEn() : zone.getName());
            imageView.setImageResource(R.drawable.ic_area);

            if (pos++ % 2 != 0) {
                row.setBackgroundResource(R.color.grey);
            }

            row.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    latitude = zone.getLatitude();
                    longitude = zone.getLongitude();

                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), CctvConstant.DEFAULT_ZOOM));
                    areaListLayout.setVisibility(View.GONE);
                }
            });

            areasLayout.addView(row);
        }
    }

    private void prepareEvents() {
        String[] titles = getResources().getStringArray(R.array.event_type_title);

        final int[] ids = {R.drawable.ic_event_all, R.drawable.ic_event_hit, R.drawable.ic_event_mob,
                R.drawable.ic_event_fix, R.drawable.ic_event_road, R.drawable.ic_event_traffic,
                R.drawable.ic_event_construct, R.drawable.ic_event_repair, R.drawable.ic_event_fire,
                R.drawable.ic_event_rain};

        final int[] idSelecteds = {R.drawable.ic_event_all_selected, R.drawable.ic_event_hit_selected,
                R.drawable.ic_event_mob_selected, R.drawable.ic_event_fix_selected, R.drawable.ic_event_road_selected,
                R.drawable.ic_event_traffic_selected, R.drawable.ic_event_construct_selected,
                R.drawable.ic_event_repair_selected, R.drawable.ic_event_fire_selected,
                R.drawable.ic_event_rain_selected};

        int pos = 0;

        for (String title : titles) {
            View row = getActivity().getLayoutInflater().inflate(R.layout.area_item, areasLayout, false);
            TextView titleView = (TextView) row.findViewById(R.id.title);
            ImageView imageView = (ImageView) row.findViewById(R.id.icon);

            titleView.setText(title);
            imageView.setImageResource(ids[pos]);

            final int position = pos;

            if (pos++ % 2 != 0) {
                row.setBackgroundResource(R.color.grey);
            }

            row.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    currentEvent = ids[position];
                    currentEventSeleted = idSelecteds[position];
                    eventBtn.setImageResource(currentEventSeleted);
                    eventListLayout.setVisibility(View.GONE);

                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(CctvConstant.DEFAULT_LATITUDE, CctvConstant.DEFAULT_LONGITUDE), 12));
                    
                    new GetCctvTask(getActivity(), map).execute(GetCctvTask.CCTV_TYPE_EVENT, position, "");
                }
            });

            eventsLayout.addView(row);
        }
    }

    private void closeInfoWindow() {
        if (currentMarker != null) {
            currentMarker.hideInfoWindow();
        }
    }
    
    private class GetCctvPhotoTask extends AsyncTask<Object, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Object... params) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL((String) params[0]).openConnection();
                connection.setDoInput(true);
                connection.connect();
                return BitmapFactory.decodeStream(connection.getInputStream());
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                cctvView.setImageBitmap(result);
            } else {
                cctvView.setImageResource(R.drawable.videoerror);
                Log.d(TAG, "Cannot load image");
            }
        }
    }

    private class GetNewsTask extends AsyncTask<String, Void, News> {

        @Override
        protected News doInBackground(String... params) {
            return SoapConnector.getNews(params[0]);
        }

        @Override
        protected void onPostExecute(News result) {
            if (result != null) {
                fullTitleView.setText(result.getHeadline());
                fullDetailView.setText(result.getDetail());
                poiBtn.setImageResource(R.drawable.ic_info);

                if (result.getRefer().length() > 40) {
                    fullReferView.setText(result.getRefer().substring(0, 40) + "...");
                } else {
                    fullReferView.setText(result.getRefer());
                }

                try {
                    Picasso.with(getActivity()).load(Uri.parse(ConnectionHelper.encodeUrlWithThaiChar(result.getImagePath()).toURI().toString())).into(fullImageView, new com.squareup.picasso.Callback() {

                        @Override
                        public void onError() {
                            Picasso.with(getActivity()).load(R.drawable.no_avatar).into(fullImageView);
                        }

                        @Override
                        public void onSuccess() {
                            // Do nothing
                        }
                        
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetEventTask extends AsyncTask<String, Void, Event> {

        @Override
        protected Event doInBackground(String... params) {
            return SoapConnector.getEvent(params[0]);
        }

        @Override
        protected void onPostExecute(Event result) {
            if (result != null) {
                fullTitleView.setText(result.getHeadline());
                fullDetailView.setText(result.getDetail());

                switch (result.getType()) {
                case 0:
                    poiBtn.setImageResource(R.drawable.ic_event_all);
                    break;
                case 1:
                    poiBtn.setImageResource(R.drawable.ic_event_hit);
                    break;
                case 2:
                    poiBtn.setImageResource(R.drawable.ic_event_mob);
                    break;
                case 3:
                    poiBtn.setImageResource(R.drawable.ic_event_fix);
                    break;
                case 4:
                    poiBtn.setImageResource(R.drawable.ic_event_road);
                    break;
                case 5:
                    poiBtn.setImageResource(R.drawable.ic_event_traffic);
                    break;
                case 6:
                    poiBtn.setImageResource(R.drawable.ic_event_construct);
                    break;
                case 7:
                    poiBtn.setImageResource(R.drawable.ic_event_repair);
                    break;
                case 8:
                    poiBtn.setImageResource(R.drawable.ic_event_fire);
                    break;
                case 9:
                    poiBtn.setImageResource(R.drawable.ic_event_rain);
                    break;
                }

                if (result.getRefer().length() > 40) {
                    fullReferView.setText(result.getRefer().substring(0, 40) + "...");
                } else {
                    fullReferView.setText(result.getRefer());
                }

                try {
                    Picasso.with(getActivity()).load(Uri.parse(ConnectionHelper.encodeUrlWithThaiChar(result.getImagePath()).toURI().toString())).into(fullImageView, new com.squareup.picasso.Callback() {

                        @Override
                        public void onError() {
                            Picasso.with(getActivity()).load(R.drawable.no_avatar).into(fullImageView);
                        }

                        @Override
                        public void onSuccess() {
                            // Do nothing
                        }
                        
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class SearchPlaceTask extends AsyncTask<String, Void, List<LatLng>> {

        @Override
        protected List<LatLng> doInBackground(String... params) {
            // Clear place markers
            //
            for (final Marker marker : searchPlaceMarkers) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        marker.remove();
                    }
                });
            }

            searchPlaceMarkers.clear();

            try {
                double latitude = CctvConstant.DEFAULT_LATITUDE;
                double longitude = CctvConstant.DEFAULT_LONGITUDE;

                Location location = getCctvActivity().getLocation();

                if (location != null && !range.equalsIgnoreCase("0")) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    return SoapConnector.searchPlace(placeView.getText().toString(), latitude, longitude, range, true);
                } else {
                    return SoapConnector.searchPlace(placeView.getText().toString(), latitude, longitude, range, false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<LatLng> result) {
            if (result != null && !result.isEmpty()) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                for (LatLng geo : result) {
                    Marker marker = map.addMarker(new MarkerOptions().position(geo));
                    searchPlaceMarkers.add(marker);
                    builder.include(marker.getPosition());
                }

                searchPlaceLayout.setVisibility(View.GONE);
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 20));
            } else {
                Toast.makeText(getActivity(), "Result not found", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(false);
        
        if (type == TYPE_EVENT || type == TYPE_INFO) {
            if (type == TYPE_EVENT) {
                new GetCctvTask(getActivity(), map).execute(GetCctvTask.CCTV_TYPE_EVENT, eventType, id);
            } else {
                new GetCctvTask(getActivity(), map).execute(GetCctvTask.CCTV_TYPE_NEWS, id);
            }

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), CctvConstant.DEFAULT_ZOOM));
        } else {
            new GetCctvTask(getActivity(), map, true).execute(GetCctvTask.CCTV_TYPE_CCTV);
            new GetCctvTask(getActivity(), map, true).execute(GetCctvTask.CCTV_TYPE_NEWS);
            new GetCctvTask(getActivity(), map, true).execute(GetCctvTask.CCTV_TYPE_EVENT, 0, "");
            
            map.setTrafficEnabled(true);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(CctvConstant.DEFAULT_LATITUDE, CctvConstant.DEFAULT_LONGITUDE), CctvConstant.DEFAULT_ZOOM));
        }

        map.setOnMapClickListener(new OnMapClickListener() {
            
            @Override
            public void onMapClick(LatLng point) {
                if (timer != null) {
                    timer.cancel();
                }

                cctvLayout.setVisibility(View.GONE);
            }
        });
        
        map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
            
            @Override
            public boolean onMyLocationButtonClick() {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()), CctvConstant.DEFAULT_ZOOM));
                return true;
            }
        });
        
        map.setInfoWindowAdapter(new InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(final Marker marker) {
                if (marker.getTitle() != null && !marker.getTitle().isEmpty()) {
                    String[] details = marker.getTitle().split(";");
                    int type = Integer.parseInt(details[0]);

                    View infoWindow = null;

                    if (type != SoapConnector.CCTV_TYPE_CCTV) {
                        infoWindow = getActivity().getLayoutInflater().inflate(R.layout.callout_info_view, null);
                        final ImageView thumbnailView = (ImageView) infoWindow.findViewById(R.id.thumbnail);
                        TextView titleView = (TextView) infoWindow.findViewById(R.id.title);
                        TextView detailView = (TextView) infoWindow.findViewById(R.id.detail);

                        if (marker.getSnippet() == null || marker.getSnippet().isEmpty()) {
                            Picasso.with(getActivity()).load(Uri.parse(details[4]))
                                    .into(thumbnailView, new com.squareup.picasso.Callback() {

                                        @Override
                                        public void onError() {
                                            Log.d(TAG, "Image error");
                                            marker.setSnippet("Image error");
                                            
                                            Picasso.with(getActivity()).load(R.drawable.no_avatar).into(thumbnailView, new com.squareup.picasso.Callback() {

                                                @Override
                                                public void onError() {
                                                    
                                                }

                                                @Override
                                                public void onSuccess() {
                                                    marker.showInfoWindow();
                                                }
                                            });
                                            
                                        }

                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Image downloaded");
                                            marker.setSnippet("Image downloaded");
                                            marker.showInfoWindow();
                                        }
                                    });
                        } else {
                            if (marker.getSnippet().equalsIgnoreCase("Image downloaded")) {
                                Picasso.with(getActivity()).load(Uri.parse(details[4])).into(thumbnailView);
                            } else {
                                Picasso.with(getActivity()).load(R.drawable.no_avatar).into(thumbnailView);
                            }
                            
                            marker.setSnippet("");
                        }

                        titleView.setText(details[2]);
                        detailView.setText(details[3]);
                    }

                    return infoWindow;
                }

                return null;
            }
        });

        map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                String[] details = marker.getTitle().split(";");
                int type = Integer.parseInt(details[0]);
                
                if (type != GetCctvTask.CCTV_TYPE_CCTV) {
                    fullDetailLayout.setVisibility(View.VISIBLE);
                    fullTitleView.setText("");
                    fullDetailView.setText("");
                    fullReferView.setText("");
                    fullImageView.setImageResource(R.drawable.no_avatar);

                    if (type == GetCctvTask.CCTV_TYPE_NEWS) {
                        new GetNewsTask().execute(details[1]);
                    } else {
                        new GetEventTask().execute(details[1]);
                    }
                }
            }
        });

        map.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                currentMarker = marker;
                
                if (marker.getTitle() != null && !marker.getTitle().isEmpty()) {
                    String[] details = marker.getTitle().split(";");
                    int type = Integer.parseInt(details[0]);

                    if (type != GetCctvTask.CCTV_TYPE_CCTV) {
                        return false;
                    } else {
                        boolean isEng = getApp().getLocale() == Locale.ENGLISH;
                        String location = isEng ? details[3] : details[2];
                        String detail = isEng ? details[6] : details[5];
                        final String ip = details[4].replace(".", "_");

                        cctvNameView.setText(location);
                        cctvDetailView.setText(detail.contains("anyType") || detail.contains("-") ? "" : detail);

                        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude)));
                        cctvLayout.setVisibility(View.VISIBLE);

                        if (timer != null) {
                            timer.cancel();
                        }

                        Picasso.with(getActivity()).load(Uri.parse(IMAGE_CCTV + details[4] + "_mark.png")).into(directionView);

                        timer = new Timer();
                        timer.scheduleAtFixedRate(new TimerTask() {

                            @Override
                            public void run() {
                                if (getCctvPhotoTask == null || getCctvPhotoTask.getStatus() != Status.RUNNING) {
                                    getCctvPhotoTask = new GetCctvPhotoTask();
                                    getCctvPhotoTask.execute(IMAGE_CCTV + ip + ".jpg");
                                }
                            }

                        }, 0, 1000);
                    }

                    return true;
                }

                return false;
            }
        });
    }
}
