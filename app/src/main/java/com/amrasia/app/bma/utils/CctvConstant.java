package com.amrasia.app.bma.utils;

/**
 * 
 * @author BabeDev
 *
 */
public final class CctvConstant {

    public static double DEFAULT_LATITUDE = 13.768705;
    public static double DEFAULT_LONGITUDE = 100.551357;
    public static float DEFAULT_ZOOM = 16;

}
