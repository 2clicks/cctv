package com.amrasia.app.bma.app;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * 
 * @author BabeDev
 * 
 */
public class CctvActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
    }

    protected CctvApplication getApp() {
        return (CctvApplication) getApplication();
    }
}
