package com.amrasia.app.bma.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * 
 * @author BabeDev
 * 
 */
public final class ImageHelper {

    /**
     * Convert from density point to pixel
     * 
     * @param context
     *            Context
     * @param dp
     *            Density point
     * @return Converted pixel
     */
    public static int dp2px(Context context, int dp) {
        return (int) (dp * (context.getResources().getDisplayMetrics().density));
    }

    public static Bitmap scaleBitmap(Bitmap bm, int size) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        if (width > size || height > size) {
            if (width > height) {
                // landscape
                int ratio = width / size;
                width = size;
                height = height / ratio;
            } else if (height > width) {
                // portrait
                int ratio = height / size;
                height = size;
                width = width / ratio;
            } else {
                // square
                height = size;
                width = size;
            }

            return Bitmap.createScaledBitmap(bm, width, height, true);
        } else {
            return bm;
        }
    }

    /**
     * To add round corner to bitmap
     * 
     * @param bitmap
     *            Bitmap
     * @param pixels
     *            Pixels of corner
     * @return Converted bitmap with round corner
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {

        try {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        } catch (Exception e) {
            return bitmap;
        }
    }
}
