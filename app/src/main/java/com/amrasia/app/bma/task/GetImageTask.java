package com.amrasia.app.bma.task;

import java.net.HttpURLConnection;
import java.net.URL;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.utils.ConnectionHelper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 * 
 * @author BabeDev
 * 
 */
public class GetImageTask extends AsyncTask<Object, Void, Bitmap> {

    private ImageView imageView;

    @Override
    protected Bitmap doInBackground(Object... params) {
        imageView = (ImageView) params[1];

        try {
            URL url = ConnectionHelper.encodeUrlWithThaiChar((String) params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            return BitmapFactory.decodeStream(connection.getInputStream());
        } catch (Exception e) {
        	e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            imageView.setImageBitmap(result);
        }
        
        imageView.setImageResource(R.drawable.ic_launcher);
    }
}
