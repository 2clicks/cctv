package com.amrasia.app.bma.flow;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvFragment;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.News;
import com.amrasia.app.bma.utils.ConnectionHelper;
import com.amrasia.app.bma.utils.ImageHelper;

/**
 * 
 * @author BabeDev
 * 
 */
public class InfoListFragment extends CctvFragment {

    private static final int THUMBNAIL_SIZE = 150;

    private LinearLayout infoLayout;
    private View listLayout;
    private View dataNotFoundView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_list, container, false);
        infoLayout = (LinearLayout) view.findViewById(R.id.info_layout);
        listLayout = view.findViewById(R.id.list_layout);
        dataNotFoundView = view.findViewById(R.id.data_not_found);

        dataNotFoundView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new ListTask().execute();
            }
        });

        new ListTask().execute();

        return view;
    }

    private class ListTask extends AsyncTask<Void, Void, List<News>> {

        private ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "", "Get information...");
        }

        @Override
        protected List<News> doInBackground(Void... params) {
            return SoapConnector.listNews();
        }

        @Override
        protected void onPostExecute(List<News> result) {
            dialog.dismiss();

            int pos = 0;

            if (!result.isEmpty()) {
                listLayout.setVisibility(View.VISIBLE);
                dataNotFoundView.setVisibility(View.GONE);

                for (final News news : result) {
                    if (getActivity() == null) {
                        break;
                    }

                    View row = getActivity().getLayoutInflater().inflate(R.layout.info_item, null, false);
                    TextView titleView = (TextView) row.findViewById(R.id.title);
                    TextView detailView = (TextView) row.findViewById(R.id.description);
                    ImageView imageView = (ImageView) row.findViewById(R.id.thumbnail);

                    if (news.getHeadline().length() > 50) {
                        titleView.setText(news.getHeadline().substring(0, 50) + "...");
                    } else {
                        titleView.setText(news.getHeadline());
                    }

                    if (news.getDetail().length() > 100) {
                        detailView.setText(news.getDetail().substring(0, 100) + "...");
                    } else {
                        detailView.setText(news.getDetail());
                    }

                    if (pos++ % 2 == 0) {
                        row.setBackgroundResource(R.color.white);
                    } else {
                        row.setBackgroundResource(R.color.grey);
                    }

                    row.setOnClickListener(new OnClickListener() {

                        public void onClick(View v) {
                            getCctvActivity().openInfoDetail(news.getId());
                        }
                    });

                    new GetThumbnailTask().executeOnExecutor(THREAD_POOL_EXECUTOR, news.getImagePath(), imageView);

                    infoLayout.addView(row);
                }
            } else {
                listLayout.setVisibility(View.GONE);
                dataNotFoundView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class GetThumbnailTask extends AsyncTask<Object, Void, Bitmap> {

        private ImageView imageView;

        @Override
        protected Bitmap doInBackground(Object... params) {
            imageView = (ImageView) params[1];

            try {
                URL url = ConnectionHelper.encodeUrlWithThaiChar((String) params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                return BitmapFactory.decodeStream(connection.getInputStream());
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                imageView.setImageBitmap(ImageHelper.scaleBitmap(result, THUMBNAIL_SIZE));
            }
        }
    }
}
