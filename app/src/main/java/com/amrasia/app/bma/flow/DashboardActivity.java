package com.amrasia.app.bma.flow;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvActivity;
import com.amrasia.app.bma.app.CctvApplication;
import com.google.android.gms.analytics.GoogleAnalytics;

/**
 * 
 * @author BabeDev
 * 
 */
public class DashboardActivity extends CctvActivity implements LocationListener {

    public static final String EXTRA_PAGE = "extraPage";

    public static final int PAGE_MAP = 1;
    public static final int PAGE_INFO = 2;
    public static final int PAGE_EVENT = 3;
    public static final int PAGE_MEMBER = 4;

    private ImageView mapBtn;
    private ImageView infoBtn;
    private ImageView eventBtn;
    private ImageView memberBtn;
    private WebView webView;
    private View contentLayout;

    private LocationManager locationManager;

    private MapFragment mapFragment;
    private EventListFragment eventListFragment;

    private int page;
    private String currentId;
    private boolean mapOpen;
    private boolean detailOpen;

    private int eventType = 0;

    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        mapBtn = (ImageView) findViewById(R.id.map_btn);
        infoBtn = (ImageView) findViewById(R.id.info_btn);
        eventBtn = (ImageView) findViewById(R.id.event_btn);
        memberBtn = (ImageView) findViewById(R.id.member_btn);
        contentLayout = findViewById(R.id.content_fragment);

        webView = (WebView) findViewById(R.id.web_view);
        webView.loadUrl(getString(R.string.about_path));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        page = getIntent().getIntExtra(EXTRA_PAGE, PAGE_MAP);

        mapFragment = new MapFragment();
        eventListFragment = new EventListFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (page == PAGE_MAP) {
            transaction.replace(R.id.content_fragment, mapFragment);
        } else if (page == PAGE_INFO) {
            transaction.replace(R.id.content_fragment, new InfoListFragment());
        } else if (page == PAGE_EVENT) {
            transaction.replace(R.id.content_fragment, eventListFragment);
        } else if (page == PAGE_MEMBER) {
            transaction.replace(R.id.content_fragment, new MemberFragment());
        }

        transaction.addToBackStack(null);
        transaction.commit();

        refreshButtons();

        getApp().getTracker(CctvApplication.TrackerName.APP_TRACKER);
    }

    @Override
    protected void onStart() {
        GoogleAnalytics.getInstance(this).reportActivityStart(this);

        super.onStart();
    }

    @Override
    protected void onStop() {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (webView.getVisibility() == View.VISIBLE) {
            webView.setVisibility(View.GONE);
            contentLayout.setVisibility(View.VISIBLE);
            return;
        } else {
            if (page == PAGE_MAP) {
                if (mapFragment.getAreaListLayout().getVisibility() == View.VISIBLE) {
                    mapFragment.getAreaListLayout().setVisibility(View.GONE);
                    return;
                } else if (mapFragment.getEventListLayout().getVisibility() == View.VISIBLE) {
                    mapFragment.getEventListLayout().setVisibility(View.GONE);
                    return;
                } else if (mapFragment.getFullDetailLayout().getVisibility() == View.VISIBLE) {
                    mapFragment.getFullDetailLayout().setVisibility(View.GONE);
                    return;
                } else if (mapFragment.getSearchPlaceLayout().getVisibility() == View.VISIBLE) {
                    mapFragment.getSearchPlaceLayout().setVisibility(View.GONE);
                    return;
                }
            } else if (page == PAGE_INFO) {
                if (mapOpen) {
                    openInfoDetail(currentId);
                    return;
                } else if (detailOpen) {
                    openInfoList();
                    return;
                }
            } else if (page == PAGE_EVENT) {
                if (mapOpen) {
                    openEventDetail(currentId);
                    return;
                } else if (detailOpen) {
                    openEventList();
                    return;
                } else if (eventListFragment.getReviewLayout().getVisibility() == View.VISIBLE) {
                    eventListFragment.getReviewLayout().setVisibility(View.GONE);
                    eventListFragment.getAddLayout().setVisibility(View.VISIBLE);
                    return;
                } else if (eventListFragment.getAddLayout().getVisibility() == View.VISIBLE) {
                    eventListFragment.getAddLayout().setVisibility(View.GONE);
                    eventListFragment.getListLayout().setVisibility(View.VISIBLE);
                    return;
                } else if (eventListFragment.getAreaListLayout().getVisibility() == View.VISIBLE) {
                    eventListFragment.getAreaListLayout().setVisibility(View.GONE);
                    eventListFragment.getAddLayout().setVisibility(View.VISIBLE);
                    return;
                }
            }
        }

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        Configuration config = new Configuration();
        config.locale = getApp().getLocale();
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onProviderDisabled(String provider) {
        // Do nothing
    }

    @Override
    public void onProviderEnabled(String provider) {
        // Do nothing
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // Do nothing
    }

    public void toggle(View v) {
        webView.setVisibility(View.GONE);
        contentLayout.setVisibility(View.VISIBLE);

        if (v.getId() == R.id.map_btn) {
            page = PAGE_MAP;

            mapFragment = new MapFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, mapFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (v.getId() == R.id.info_btn) {
            page = PAGE_INFO;

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, new InfoListFragment());
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (v.getId() == R.id.event_btn) {
            page = PAGE_EVENT;

            eventListFragment = new EventListFragment();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, eventListFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (v.getId() == R.id.member_btn) {
            page = PAGE_MEMBER;

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, new MemberFragment());
            transaction.addToBackStack(null);
            transaction.commit();
        }

        refreshButtons();
    }

    public void gotoMenu(View v) {
        finish();
    }

    public void openAbout(View v) {
        webView.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.GONE);
    }

    public void openMap(String id, double latitude, double longitude, int type, String title, String detail,
            Bitmap thumbnail) {
        mapOpen = true;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, new MapFragment(id, latitude, longitude, type, title, detail,
                thumbnail));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openMap(String id, int eventType, double latitude, double longitude, int type, String title,
            String detail, Bitmap thumbnail) {
        mapOpen = true;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, new MapFragment(id, eventType, latitude, longitude, type, title,
                detail, thumbnail));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openInfoDetail(String id) {
        currentId = id;
        mapOpen = false;
        detailOpen = true;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, new InfoDetailFragment(id));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openInfoList() {
        detailOpen = false;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, new InfoListFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openEventDetail(String id) {
        currentId = id;
        mapOpen = false;
        detailOpen = true;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, new EventDetailFragment(id));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void openEventList() {
        detailOpen = false;

        eventListFragment = new EventListFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, eventListFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public int getEventType() {
        return eventType;
    }

    public Location getLocation() {
        return location;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    private void refreshButtons() {
        mapBtn.setImageResource(R.drawable.map_btn);
        infoBtn.setImageResource(R.drawable.info_btn);
        eventBtn.setImageResource(R.drawable.event_btn);
        memberBtn.setImageResource(R.drawable.member_btn);

        switch (page) {
        case PAGE_MAP:
            mapBtn.setImageResource(R.drawable.map_btn_selected);
            break;
        case PAGE_INFO:
            infoBtn.setImageResource(R.drawable.info_btn_selected);
            break;
        case PAGE_EVENT:
            eventBtn.setImageResource(R.drawable.event_btn_selected);
            break;
        case PAGE_MEMBER:
            memberBtn.setImageResource(R.drawable.member_btn_selected);
            break;
        }
    }

}
