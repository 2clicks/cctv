package com.amrasia.app.bma.flow;

import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.amrasia.app.bma.R;
import com.amrasia.app.bma.app.CctvActivity;
import com.amrasia.app.bma.connector.SoapConnector;
import com.amrasia.app.bma.model.Area;
import com.amrasia.app.bma.model.Zone;

/**
 * 
 * @author BabeDev
 * 
 */
public class MenuActivity extends CctvActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = new Configuration();
        config.locale = new Locale("th", "TH");
        getApp().setLocale(config.locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        setContentView(R.layout.landing);

        new GetAreaList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetZoneList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void open(View v) {
        Intent intent = new Intent(this, DashboardActivity.class);

        switch (v.getId()) {
        case R.id.map_btn:
            intent.putExtra(DashboardActivity.EXTRA_PAGE, DashboardActivity.PAGE_MAP);
            break;
        case R.id.info_btn:
            intent.putExtra(DashboardActivity.EXTRA_PAGE, DashboardActivity.PAGE_INFO);
            break;
        case R.id.event_btn:
            intent.putExtra(DashboardActivity.EXTRA_PAGE, DashboardActivity.PAGE_EVENT);
            break;
        case R.id.member_btn:
            intent.putExtra(DashboardActivity.EXTRA_PAGE, DashboardActivity.PAGE_MEMBER);
            break;
        }

        startActivity(intent);
    }

    public void setThai(View v) {
        Configuration config = new Configuration();
        config.locale = new Locale("th", "TH");
        getApp().setLocale(config.locale);
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.landing);
    }

    public void setEnglish(View v) {
        Configuration config = new Configuration();
        config.locale = Locale.ENGLISH;
        getApp().setLocale(config.locale);
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.landing);
    }

    private class GetAreaList extends AsyncTask<Void, Void, List<Area>> {

        @Override
        protected List<Area> doInBackground(Void... params) {
            return SoapConnector.listAreas();
        }

        @Override
        protected void onPostExecute(List<Area> result) {
            if (result != null) {
                getApp().getAreas().addAll(result);
            }
        }
    }

    private class GetZoneList extends AsyncTask<Void, Void, List<Zone>> {

        @Override
        protected List<Zone> doInBackground(Void... params) {
            return SoapConnector.listZones();
        }

        @Override
        protected void onPostExecute(List<Zone> result) {
            if (result != null) {
                getApp().getZones().addAll(result);
            }
        }
    }
}
