/**
 * @author Giri Jeedigunta
 * Visit: http://thewebstorebyg.wordpress.com/ for more tutorials on maps
 */

var direction_type = 1;
var tog=0;
var ip=0;

(function(mapDemo, $, undefined) {
	
	
	
	
	
	mapDemo.Directions = (function() {
		function _Directions() {
                          
            $("#drivenRouteImg").attr("src","cb_active.png");
			
			$(".closedirection").hide();
                          
                          $(".openmapbtn").hide();
                          
                          $("#paneToggle").hide();
			var map,   
				directionsService, directionsDisplay, 
				autoSrc, autoDest, pinA, pinB, 
				
				markerA = new google.maps.MarkerImage('m1.png',
						new google.maps.Size(32, 36),
						new google.maps.Point(0, 0),
						new google.maps.Point(10, 32)),		
				markerB = new google.maps.MarkerImage('m2.png',
						new google.maps.Size(32, 36),
						new google.maps.Point(0, 0),
						new google.maps.Point(10, 32)), 
				
				// Caching the Selectors		
				$Selectors = {
					mapCanvas: jQuery('#mapCanvas')[0], 
					dirPanel: jQuery('#directionsPanel'),
					dirInputs: jQuery('.directionInputs'),
					dirSrc: jQuery('#dirSource'),
					dirDst: jQuery('#dirDestination'),
					getDirBtn: jQuery('#getDirections2'),
					dirSteps: jQuery('#directionSteps'), 
					paneToggle: jQuery('#paneToggle'),
                    paneToggle2: jQuery('#paneToggle2'),
					paneToggle3: jQuery('#paneToggle3'),
                    getDir : jQuery('#getDir'),    
					useGPSBtn: jQuery('#useGPS'), 
					paneResetBtn: jQuery('#paneReset'),
					switchPlace: jQuery('#switchPlace'),
					closedirection: jQuery('#closedirection'),
					listDirection: jQuery('#directionSteps ')
				},
				
				autoCompleteSetup = function() {
					autoSrc = new google.maps.places.Autocomplete($Selectors.dirSrc[0]);
					autoDest = new google.maps.places.Autocomplete($Selectors.dirDst[0]);
				}, // autoCompleteSetup Ends
			
				directionsSetup = function() {
					directionsService = new google.maps.DirectionsService();
					directionsDisplay = new google.maps.DirectionsRenderer({
						suppressMarkers: true
					});	
					
					directionsDisplay.setPanel($Selectors.dirSteps[0]);											
				}, // direstionsSetup Ends
				
				trafficSetup = function() {					
					// Creating a Custom Control and appending it to the map
					var controlDiv = document.createElement('div'), 
						controlUI = document.createElement('div'), 
						trafficLayer = new google.maps.TrafficLayer();
							
					jQuery(controlDiv).addClass('gmap-control-container').addClass('gmnoprint');
					jQuery(controlUI).text('Traffic').addClass('gmap-control');
					 
					jQuery(controlDiv).append(controlUI);
								
							
					// Traffic Btn Click Event	  
					google.maps.event.addDomListener(controlUI, 'click', function() {
						if (typeof trafficLayer.getMap() == 'undefined' || trafficLayer.getMap() === null) {
							jQuery(controlUI).addClass('gmap-control-active');
							trafficLayer.setMap(map);
							
							
							
							
							
						} else {
							trafficLayer.setMap(null);
							jQuery(controlUI).removeClass('gmap-control-active');
							
						}
					});							  
					map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);								
				}, // trafficSetup Ends
				
				mapSetup = function() {					
					map = new google.maps.Map($Selectors.mapCanvas, {
							zoom: 9,
							center: new google.maps.LatLng(42.44, -83.18),	
							
		                     mapTypeControl: false,
		                    mapTypeControlOptions: {
		                        style: google.maps.MapTypeControlStyle.DEFAULT,
		                       // position: google.maps.ControlPosition.TOP_RIGHT
		                    },
		
		                   /* panControl: true,
		                    panControlOptions: {
		                        position: google.maps.ControlPosition.RIGHT_TOP
		                    },
		*/
		                    zoomControl: false,
		                    zoomControlOptions: {
		                        style: google.maps.ZoomControlStyle.SMALL,
		                        position: google.maps.ControlPosition.RIGHT_TOP
		                    },
		                    
		                    
						    streetViewControl: false, 
							/*overviewMapControl: true,*/
							/*disableDefaultUI: true,*/
							scaleControl: false, 
							 disableDefaultUI: true,							
							mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					
					autoCompleteSetup();
					directionsSetup();
					trafficSetup();
				}, // mapSetup Ends 
				
				directionsRender = function(source, destination) {
					//$Selectors.dirSteps.find('.msg').hide();
					directionsDisplay.setMap(map);
					
					var request = {
						origin: source,
						destination: destination,
						provideRouteAlternatives: false, 
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					};
                          
                          if( direction_type == 2){
                          request = {
                          origin: source,
                          destination: destination,
                          provideRouteAlternatives: false,
                          travelMode: google.maps.DirectionsTravelMode.TRANSIT
                          };
                          }
                          
					//travelMode: google.maps.DirectionsTravelMode.DRIVING
					directionsService.route(request, function(response, status) {
						if (status == google.maps.DirectionsStatus.OK) {

							directionsDisplay.setDirections(response);
							
							var _route = response.routes[0].legs[0]; 
							
							pinA = new google.maps.Marker({position: _route.start_location, map: map, icon: markerA}), 
							pinB = new google.maps.Marker({position: _route.end_location, map: map, icon: markerB});																	
						}
					});
					
				}, // directionsRender Ends
				
				fetchAddress = function(p) {
					var Position = new google.maps.LatLng(p.coords.latitude, p.coords.longitude),  
						Locater = new google.maps.Geocoder();
					
					Locater.geocode({'latLng': Position}, function (results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							var _r = results[0];
							$Selectors.dirSrc.val(_r.formatted_address);
						}
					});
				}, // fetchAddress Ends
				
				
					
					
				invokeEvents = function() {
                          
                          $("#drivenRouteImg").on('click', function(e) {
                                                  direction_type = 1;
                                                  $("#drivenRouteImg").attr("src","cb_active.png");
                                                  $("#transitRouteImg").attr("src","cb.png");
                                                  });
                          $("#transitRouteImg").on('click', function(e){
                                                  direction_type = 2;
                                                  $("#drivenRouteImg").attr("src","cb.png");
                                                  $("#transitRouteImg").attr("src","cb_active.png");
                                                  });

					
					$Selectors.switchPlace.on('click', function(e) {
						e.preventDefault();
						var src = $Selectors.dirSrc.val(), 
							dst = $Selectors.dirDst.val();
							
						$Selectors.dirDst.val(src);
						$Selectors.dirSrc.val(dst);
						 
						//directionsRender(src, dst);
					});
					
					// Get Directions
					$Selectors.getDirBtn.click( function(e) {
						
						
						 
						
						e.preventDefault();
						var src = $Selectors.dirSrc.val(), 
						    dst = $Selectors.dirDst.val();
					if(src !='' && dst !=''){
						directionsRender(src, dst); 
						//alert("xx")
						//$Selectors.dirInputs.hide();
						
						$Selectors.dirSteps.show();
						$(".closedirection").show();
                                            $(".openmapbtn").show();
                                              $("#paneToggle").show();
						$Selectors.dirInputs.slideUp( "200", function() {
    // Animation complete.
							
							$Selectors.dirSteps.height('100%');
							 
  						});
						
						}else if(src==''){
							
							$Selectors.dirSrc.focus();
							
						}else if(dst==''){
	
							$Selectors.dirDst.focus();
							
						}
						
					});
					
					// Get Directions
					$Selectors.closedirection.on('click', function(e) { 
						//alert("closedirection");
						e.preventDefault();
						//$Selectors.dirInputs.show();
						//$Selectors.dirSteps.height('100%');
						$(".closedirection").hide();
                                           //      $(".closedirection").hide();
                                                 
                                                 $(".openmapbtn").hide();
                                                  $("#paneToggle").hide();
						$Selectors.dirInputs.slideDown( "200", function() {
    // Animation complete.
							//$Selectors.dirSteps.hide();
							$Selectors.dirSteps.fadeOut( 200, function(e) {} );
  						});
					});
					
					
					
					// Reset Btn
					$Selectors.paneResetBtn.on('click', function(e) {
						$Selectors.dirSteps.html('');
						$Selectors.dirSrc.val('');
						$Selectors.dirDst.val('');
						
						if(pinA) pinA.setMap(null);
						if(pinB) pinB.setMap(null);		
						
						directionsDisplay.setMap(null);					
					});
					
					
				  $Selectors.listDirection.on('click', function(e) {
					 $Selectors.paneToggle.click();
					 
					 
					});
					
					// Toggle Btn
					/*$Selectors.paneToggle.toggle(function(e) {
						
						$Selectors.dirPanel.animate({'left': '-=80%'});
						jQuery(this).html('<img src="direct_icon.png" border="0" width="56"/>');
						
					}, function() {
						
						$Selectors.dirPanel.animate({'left': '+=80%'});
						jQuery(this).html('<img src="route_icon.png" border="0" width="56"/>');
						
						
					});*/
					
					
					
					$Selectors.paneToggle.on('click', function(e) {
						
						if(tog==0){
						
						$Selectors.dirPanel.animate({'left': '-=100%'});
						$Selectors.getDir.fadeIn( 200, function(e) {} );
						//$Selectors.paneToggle.html('<img src="direct_icon.png" border="0" width="56"/>');
						//$Selectors.paneToggle.animate({'right': '-75px'});
						 tog=1;
						}else{
					  
						$Selectors.dirPanel.animate({'left': '+=100%'});
						$Selectors.getDir.fadeOut( 200, function(e) {} );
						//jQuery(this).html('<img src="route_icon.png" border="0" width="56"/>');
						//jQuery(this).animate({'right': '0px'});
						
						 tog=0;
						}
						
					});
                          
                          $Selectors.paneToggle3.on('click', function(e) {
                                                  //  alert("click")
                                                    // Toggle Btn
                                                    $Selectors.paneToggle.click();
                                                  });
                          
                   
					
					// Use My Location / Geo Location Btn
					$Selectors.useGPSBtn.on('click', function(e) {	
					
					    $Selectors.dirSrc.val('');
						 if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								 fetchAddress(position);
							});	
						} 
						
					 
						 
						
					});
				}, //invokeEvents Ends 
				
				_init = function() {
					mapSetup();
					invokeEvents();
					//switchSD();
				}; // _init Ends
				
			this.init = function() {
				_init();
				return this; // Refers to: mapDemo.Directions
			}
			return this.init(); // Refers to: mapDemo.Directions.init()
		} // _Directions Ends
		return new _Directions(); // Creating a new object of _Directions rather than a funtion
	}()); // mapDemo.Directions Ends
	
	
	 
	
})(window.mapDemo = window.mapDemo || {}, jQuery);
